package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.ComplexDTO;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface ComplexService {
    Optional<ComplexDTO> getComplex(Long id);
    Optional<ComplexDTO> getComplex(String name);

    Collection<ComplexDTO> getAllComplexes();

    List<ComplexDTO> getComplexesByDeveloper(Long id);

    ComplexDTO save(ComplexDTO complex);

    void delete(Long id);
}
