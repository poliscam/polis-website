package ru.poliscam.website.services;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 09.11.18
 */
public interface EmailService {
    void sendText(String email, String text);
}
