package ru.poliscam.website.services;

import org.springframework.data.domain.PageRequest;
import ru.poliscam.website.models.dto.NewsDTO;

import java.util.List;
import java.util.Optional;

public interface NewsService {
    List<NewsDTO> getNews(PageRequest page);
    Optional<NewsDTO> getLastNews();
    Optional<NewsDTO> addNews(NewsDTO news);
    Optional<NewsDTO> getNews(Long id);
}
