package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.poliscam.website.jobs.PopulateSearchCacheJob;
import ru.poliscam.website.models.dto.SearchedObjectDTO;
import ru.poliscam.website.models.elastic.SearchedObject;
import ru.poliscam.website.services.SearchService;

import java.lang.reflect.Type;
import java.util.List;

@Service
@Log4j2
public class SearchServiceImpl implements SearchService {
    private final static Integer MAXIMUM_OBJECTS_FROM_SEARCH = 10;

    private final static String INDEX = "search";
    private final static String NAME_FIELD = "name";
    private final static String VIEWS_FIELD = "views";
    private final static String TYPE_FIELD = "type";

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ElasticsearchTemplate template;

    @Autowired
    private PopulateSearchCacheJob populateSearchCacheJob;

    private Type type = new TypeToken<List<SearchedObjectDTO>>() { }.getType();

    @Override
    public List<SearchedObjectDTO> getResults(String q) {
        SearchQuery query = new NativeSearchQueryBuilder()
                .withQuery(QueryBuilders.multiMatchQuery(q, NAME_FIELD))
                .withSort(SortBuilders.fieldSort(VIEWS_FIELD).order(SortOrder.DESC))
                .withSort(SortBuilders.fieldSort(TYPE_FIELD).order(SortOrder.ASC))
                .withIndices(INDEX)
                .withTypes(INDEX)
                .withPageable(PageRequest.of(0, MAXIMUM_OBJECTS_FROM_SEARCH))
                .build();

        Page<SearchedObject> result = template.queryForPage(query, SearchedObject.class);

        return modelMapper.map(result.getContent(), type);
    }

    @Override
    @Async
    public void reload() {
        populateSearchCacheJob.run();
    }
}
