package ru.poliscam.website.services.impl;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poliscam.website.enums.VideoType;
import ru.poliscam.website.models.dto.VideoDTO;
import ru.poliscam.website.models.sql.Video;
import ru.poliscam.website.repositories.VideoDAO;
import ru.poliscam.website.services.VideoService;

import java.lang.reflect.Type;
import java.time.Duration;
import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Service
public class VideoServiceImpl implements VideoService {
    @Autowired
    private VideoDAO videoDAO;

    @Autowired
    private ModelMapper mapper;

    private Type listVideoType = new TypeToken<List<VideoDTO>>() {
    }.getType();

    @Override
    public List<VideoDTO> getVideos(Long id, Date date, VideoType type) {
        Date stop = Date.from(date.toInstant().plus(Duration.ofDays(1L)));
        List<Video> videos = videoDAO.findByCameraIdAndTypeAndAddedBetween(id, type, date, stop);
        return mapper.map(videos, listVideoType);
    }
}
