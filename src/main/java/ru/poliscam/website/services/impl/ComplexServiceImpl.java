package ru.poliscam.website.services.impl;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.sql.Complex;
import ru.poliscam.website.repositories.ComplexDAO;
import ru.poliscam.website.services.ComplexService;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Service
public class ComplexServiceImpl implements ComplexService {
    @Autowired
    private ComplexDAO complexDAO;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BackendCache<Long, ComplexDTO> cache;

    @PostConstruct
    public void init() {
        Converter<Complex, ComplexDTO> converter = context -> {
            Set<ComplexDTO.CameraShortDTO> cameras = new TreeSet<>();

            if (!CollectionUtils.isEmpty(context.getSource().getCameras())) {
                context.getSource().getCameras().forEach(camera -> cameras.add(
                        ComplexDTO.CameraShortDTO.builder()
                                .id(camera.getId())
                                .name(camera.getName())
                                .enabled(camera.getEnabled())
                                .archive(camera.getArchive())
                                .build()
                ));
            }

            context.getDestination().setCameras(cameras);
            return context.getDestination();
        };

        mapper.createTypeMap(Complex.class, ComplexDTO.class).setPreConverter(converter);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public Optional<ComplexDTO> getComplex(Long id) {
        Optional<ComplexDTO> complexFromCache = cache.get(id);

        if (complexFromCache.isPresent()) {
            return complexFromCache;
        } else {
            Optional<Complex> complexFromDb = complexDAO.findById(id);

            if (!complexFromDb.isPresent()) {
                return Optional.empty();
            } else {
                Complex complex = complexFromDb.get();
                ComplexDTO complexDTO = mapper.map(complex, ComplexDTO.class);
                cache.put(complexDTO.getId(), complexDTO);

                return Optional.of(complexDTO);
            }
        }
    }

    @Override
    @SuppressWarnings("Duplicates")
    public Optional<ComplexDTO> getComplex(String name) {
        Optional<Complex> complexFromDb = complexDAO.findByName(name);

        if (!complexFromDb.isPresent()) {
            return Optional.empty();
        } else {
            Complex complex = complexFromDb.get();
            ComplexDTO complexDTO = mapper.map(complex, ComplexDTO.class);
            cache.put(complexDTO.getId(), complexDTO);

            return Optional.of(complexDTO);
        }
    }

    @Override
    public Collection<ComplexDTO> getAllComplexes() {
        int sizeInDb = (int) complexDAO.count();
        int sizeInCache = cache.getAll().size();

        if (sizeInDb > 0 && sizeInCache != sizeInDb) {
            List<Complex> complexes = complexDAO.findAll();
            complexes.forEach(complex -> {
                ComplexDTO complexDTO = mapper.map(complex, ComplexDTO.class);
                cache.put(complexDTO.getId(), complexDTO);
            });

            return cache.getAll().values();
        } else {
            return cache.getAll().values();
        }
    }


    @Override
    public List<ComplexDTO> getComplexesByDeveloper(Long id) {
        List<ComplexDTO> complexDTOList = new ArrayList<>();
        complexDAO.findByEnabledTrueAndDeveloperId(id).forEach(complex -> {
            ComplexDTO dto = mapper.map(complex, ComplexDTO.class);
            cache.put(complex.getId(), dto);
            complexDTOList.add(dto);
        });
        return complexDTOList;
    }

    @Override
    public ComplexDTO save(ComplexDTO complex) {
        Complex complexDb = complexDAO.save(mapper.map(complex, Complex.class));
        complex = mapper.map(complexDb, ComplexDTO.class);
        cache.put(complex.getId(), complex);
        return complex;
    }

    @Override
    public void delete(Long id) {
        complexDAO.deleteById(id);
        cache.evict(id);
    }
}
