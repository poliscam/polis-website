package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.util.HtmlUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.Page;
import ru.poliscam.website.models.dto.CommentDTO;
import ru.poliscam.website.models.sql.Account;
import ru.poliscam.website.models.sql.Comment;
import ru.poliscam.website.models.sql.Complex;
import ru.poliscam.website.repositories.AccountDAO;
import ru.poliscam.website.repositories.CommentDAO;
import ru.poliscam.website.repositories.ComplexDAO;
import ru.poliscam.website.services.CommentService;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Service
@Log4j2
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private ComplexDAO complexDAO;

    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private BackendCache<Long, Set<CommentDTO>> commentsCache;

    @Autowired
    private ModelMapper mapper;

    @Value("${comment.id.contacts}")
    private Long commentId;

    private Type listCommentType = new TypeToken<Set<CommentDTO>>() {
    }.getType();

    @Override
    public void addComment(CommentDTO commentDTO) {
        Account account = accountDAO.findByEmail(commentDTO.getAccount().getEmail());

        if(account == null) {
            log.error("No user found with email {}", commentDTO.getAccount().getEmail());
            return;
        }

        Comment comment = new Comment();
        comment.setAdded(new Date());
        comment.setIp(commentDTO.getIp());
        comment.setAccount(account);
        comment.setText(HtmlUtils.htmlEscape(commentDTO.getText()));

        if (!commentDTO.getComplexId().equals(commentId)) {
            Optional<Complex> complexOptional = complexDAO.findById(commentDTO.getComplexId());

            if (!complexOptional.isPresent()) {
                throw new IllegalArgumentException("Complex unknown!");
            } else {
                comment.setComplex(complexOptional.get());
            }
        } else {
            comment.setSpecialId(commentId);
        }

        comment = commentDAO.save(comment);
        CommentDTO commentDTOtoCache = mapper.map(comment, CommentDTO.class);

        if(comment.getSpecialId() != null) {
            commentDTOtoCache.setComplexId(comment.getSpecialId());
        }

        Optional<Set<CommentDTO>> commentDTOListOpt = commentsCache.get(commentDTO.getComplexId());

        Set<CommentDTO> commentList = commentDTOListOpt.orElseGet(TreeSet::new);
        commentList.add(commentDTOtoCache);
        commentsCache.put(commentDTO.getComplexId(), commentList);
    }

    @Override
    public void deleteComment(Long cid, Long id) {
        Optional<Set<CommentDTO>> commentDTOSetOpt = commentsCache.get(cid);

        commentDTOSetOpt.ifPresent(commentDTOS -> commentsCache.put(cid, commentDTOS.stream()
                .filter(c -> !c.getId().equals(id))
                .collect(Collectors.toSet())
        ));

        commentDAO.deleteById(id);
    }

    @Override
    public Optional<CommentDTO> getComment(Long id) {

        try {
            Optional<CommentDTO> commentDTOOptional = commentsCache.getAll().values().stream()
                    .filter(s -> s != null && !CollectionUtils.isEmpty(s))
                    .flatMap(Collection::stream)
                    .filter(c -> c.getId().equals(id))
                    .findFirst();

            if (commentDTOOptional.isPresent()) {
                return commentDTOOptional;
            }
        } catch (ClassCastException ignored) { // hazelcast generate ClassCastException in case of set is absent
        }

        Optional<Comment> commentOptional = commentDAO.findById(id);

        if(commentOptional.isPresent()) {
            Comment comment = commentOptional.get();
            CommentDTO commentDTO = mapper.map(comment, CommentDTO.class);

            if(comment.getSpecialId() != null) {
                commentDTO.setComplexId(comment.getSpecialId());
            }

            Long complexId = comment.getSpecialId() != null ? comment.getSpecialId() : comment.getComplex().getId();
            Optional<Set<CommentDTO>> commentsInCache = commentsCache.get(complexId);

            if(!commentsInCache.isPresent()) {
                Set<CommentDTO> newComment = new TreeSet<>();
                newComment.add(commentDTO);
                commentsCache.put(complexId, newComment);
            } else {
                Set<CommentDTO> comments = commentsInCache.get();
                comments = comments.stream()
                        .filter(c -> !c.getId().equals(commentDTO.getId()))
                        .collect(Collectors.toSet());

                comments.add(commentDTO);

                commentsCache.put(complexId, comments);
            }

            return Optional.of(commentDTO);
        } else {
            log.error("Comment #{} not found!", id);
        }

        return Optional.empty();
    }

    @Override
    public CommentDTO saveComment(CommentDTO toSave) {
        Account account = accountDAO.findByEmail(toSave.getAccount().getEmail());

        if(account == null) {
            log.error("No user found with email {}", toSave.getAccount().getEmail());
            return null;
        }

        Optional<Complex> complexOpt = Optional.empty();
        if(!toSave.getComplexId().equals(commentId)) {
            complexOpt = complexDAO.findById(toSave.getComplexId());

            if (!complexOpt.isPresent()) {
                log.error("No complex found with id {}", toSave.getComplexId());
                return null;
            }
        }

        Comment comment = new Comment();

        comment.setAccount(account);
        comment.setAdded(toSave.getAdded());

        if(!toSave.getComplexId().equals(commentId)) {
            comment.setComplex(complexOpt.get());
        } else {
            comment.setSpecialId(commentId);
        }

        comment.setText(toSave.getText());
        comment.setReply(toSave.getReply());
        comment.setReplyAdded(toSave.getReplyAdded());
        comment.setId(toSave.getId());
        comment.setIp(toSave.getIp());

        comment = commentDAO.save(comment);
        CommentDTO commentDTO = mapper.map(comment, CommentDTO.class);

        Optional<Set<CommentDTO>> commentsInCache = commentsCache.get(toSave.getComplexId());

        if(!commentsInCache.isPresent()) {
            Set<CommentDTO> comments = new TreeSet<>();
            if(comment.getSpecialId() != null) {
                comments.addAll(
                        commentDAO.findByComplexIdOrderByAddedDesc(comment.getSpecialId()).stream()
                                .map(c -> mapper.map(c, CommentDTO.class))
                                .collect(Collectors.toList())
                );
            } else {
                comments.addAll(commentDAO.findByComplexIdOrderByAddedDesc(comment.getComplex().getId()).stream()
                        .map(c -> mapper.map(c, CommentDTO.class))
                        .collect(Collectors.toList()));
            }
            comments.add(commentDTO);
            commentsCache.put(toSave.getComplexId(), comments);
        } else {
            Set<CommentDTO> comments = commentsInCache.get();
            comments = comments.stream()
                    .filter(c -> !c.getId().equals(commentDTO.getId()))
                    .collect(Collectors.toSet());

            comments.add(commentDTO);

            commentsCache.put(toSave.getComplexId(), comments);
        }

        return commentDTO;
    }

    @Override
    public Long getCommentsCount(Long id) {
        Optional<Set<CommentDTO>> commentDTOListOpt = commentsCache.get(id);
        Long count;

        if (commentDTOListOpt.isPresent()) {
            return (long) commentDTOListOpt.get().size();
        } else {
            if (!id.equals(commentId)) {
                count = commentDAO.countByComplexId(id);
            } else {
                count = commentDAO.countBySpecialId(id);
            }

        }
        return count != null ? count : 0L;
    }

    @Override
    public Set<CommentDTO> getCommentByComplex(Long id, Page page) {
        Optional<Set<CommentDTO>> commentDTOListOpt = commentsCache.get(id);
        Set<CommentDTO> comments = new TreeSet<>();
        int startIndex = page.getPage() <= 1 ? 0 : (page.getPage() * page.getSize()) - 1;
        int endIndex = (page.getPage() + 1) * page.getSize();

        if (commentDTOListOpt.isPresent()) {
            comments = commentDTOListOpt.get();
        } else {
            List<Comment> commentsFromDb;
            if (!id.equals(commentId)) {
                commentsFromDb = commentDAO.findByComplexIdOrderByAddedDesc(id);
            } else {
                commentsFromDb = commentDAO.findBySpecialIdOrderByAddedDesc(id);
            }

            if (!CollectionUtils.isEmpty(commentsFromDb)) {
                Set<CommentDTO> commentsAll = new HashSet<>();
                commentsFromDb.forEach(comment -> {
                    CommentDTO commentDTO = mapper.map(comment, CommentDTO.class);
                    if(comment.getSpecialId() != null) {
                        commentDTO.setComplexId(comment.getSpecialId());
                    }
                    commentsAll.add(commentDTO);
                });

                commentsCache.put(id, commentsAll);
                comments = commentsAll;
            }
        }

        if (startIndex > comments.size() - 1) {
            return Collections.emptySet();
        }

        if (endIndex > comments.size() + 1) {
            endIndex = comments.size();
        }

        return new TreeSet<>(new ArrayList(comments).subList(startIndex, endIndex));
    }


}
