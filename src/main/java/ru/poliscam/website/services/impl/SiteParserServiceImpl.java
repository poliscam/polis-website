package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.codec.binary.Base64;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.poliscam.website.models.dto.CityDTO;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.models.dto.LocationDTO;
import ru.poliscam.website.services.*;
import ru.poliscam.website.utils.HTMLTools;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Log4j2
public class SiteParserServiceImpl implements SiteParserService {

    @Autowired
    private ComplexService complexService;

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private LocationService locationService;

    @Autowired
    private SearchService searchService;

    @Override
    public Optional<ComplexDTO> getComplex(String url) {
        log.info("[parser] Starting parsing complex...");

        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException e) {
            log.error("[parser] Connection error!");
            try {
                Thread.sleep(5000L);
                doc = Jsoup.connect(url).get();
            } catch (IOException e1) {
                log.error("[parser] Connection error #2!");
                return Optional.empty();
            } catch (InterruptedException e1) {
                return Optional.empty();
            }
        }

        Elements name = doc.select("h1[itemprop = name]");

        log.info("[parser] Complex name: " + name.text());

        ComplexDTO complex = new ComplexDTO();
        Optional<ComplexDTO> complexDTOOpt = complexService.getComplex(name.text());

        if(complexDTOOpt.isPresent()) {
            log.info("[parser] Complex exists in our db");
            complex = complexDTOOpt.get();
        }
        else {
            complex.setName(name.text());
            complex = complexService.save(complex);
        }

        Element price = doc.selectFirst("div.build-info-price p.f");

        if(price != null) {
            complex.setPriceRange(price.text().replaceAll("(.*?)\\sa", "$1 руб"));
        }

        complex.setMetro(doc.selectFirst("div.build-info-metro a").text());
        complex.setAddress(doc.selectFirst("div.build-info-address a").text());
        complex.setReadyAt(doc.selectFirst("table.build-info-queue tbody tr td div").text().replaceAll("(.*?)\\s\\(под.*", "$1"));
        complex.setBuildingClass(doc.selectFirst("div.build-info-metro a").text());

        Elements elements = doc.select("div.build-info-col-left table tbody tr");
        for(Element tr : elements) {
            Elements tds = tr.select("td");
            String key = tds.get(0).text();
            String value = tds.get(1).text();

            switch (key) {
                case "Класс жилья": {
                    complex.setBuildingClass(value);
                    break;
                }
                case "Тип здания": {
                    complex.setBuildingType(value);
                    break;
                }
                case "Этажность": {
                    complex.setFloors(value);
                    break;
                }
                case "Паркинг": {
                    complex.setParking(value);
                    break;
                }
                case "Отделка:": {
                    complex.setCover(value);
                    break;
                }
                case "Разрешение на строительство": {
                    break;
                }
                default: {
                    log.error("Unknown property in description: {}", key);
                }
            }
        }

        Element element = doc.select("div.build-info-col").get(1);
        Elements trs = element.select("table tbody tr");
        for(Element tr : trs) {
            Elements tds = tr.select("td");
            String key = tds.get(0).text();
            String value = tds.get(1).text();

            switch (key) {
                case "Договор": {
                    complex.setContractType(value);
                    break;
                }
                case "Застройщик:": {
                    Element dev = tds.get(1).selectFirst("a");
                    DeveloperDTO developer = populateDeveloper(dev.text(), dev.attr("href"));
                    complex.setDeveloper(developer);
                    break;
                }
                case "Аккредитация":
                case "Обновлено":
                case "Оплата": {
                    break;
                }
                default: {
                    log.error("Unknown property in description: {}", key);
                }
            }
        }

        String locationString = doc.selectFirst("a.BuildMap").text();
        Optional<LocationDTO> locationOpt = locationService.getLocation(locationString);
        LocationDTO location = locationOpt.orElseGet(() -> createLocation(locationString));
        complex.setLocation(location);

        if(url.contains("mskguru")) {
            complex.setCity(CityDTO.builder()
                    .id(2L)
                    .build());
        } else {
            complex.setCity(CityDTO.builder()
                    .id(1L)
                    .build());
        }

        // Описание
        Elements desc = doc.select("#BuildDescription");

        Whitelist whitelist = new Whitelist();
        whitelist.addTags("p", "h4", "<br>");

        complex.setDescription(Jsoup.clean(desc.html(), whitelist));

        if(StringUtils.isEmpty(complex.getLogo())) {
            Elements imgs = doc.select("div.slider_cnt ul li img");

            if(imgs.size() >= 2) {
                String imgsrc = imgs.get(1).attr("src");

                log.info("[parser] Complex logo URL: " + imgsrc);

                BufferedImage originalImage;
                try {
                    if (imgsrc.contains("http://") || imgsrc.contains("https://")) {
                        originalImage = ImageIO.read(new URL(imgsrc));
                    } else if(imgsrc.contains("base64")) {
                        originalImage = decodeBase64Image(imgsrc);
                    }
                    else {
                        log.error("[parser] No image!");
                        return Optional.empty();
                    }
                } catch (Exception e) {
                    log.error("[parser] Error while image converting", e);
                    return Optional.empty();
                }

                int type = BufferedImage.TYPE_3BYTE_BGR;

                BufferedImage resizeImageJpg = resizeImage(originalImage, type);
                try {
                    ImageIO.write(resizeImageJpg, "jpg", new File("html/static/images/complexes/complex-" + complex.getId() + ".jpg"));
                } catch (IOException e) {
                    log.error("[parser] Error while writting image to disk", e);
                    return Optional.empty();
                }

                complex.setLogo("complex-" + complex.getId() + ".jpg");
            }
        }

        complex.setSeoName(HTMLTools.translitAndTrim(complex.getName()));
        complex.setEnabled(true);

        complexService.save(complex);

        searchService.reload();

        return Optional.of(complex);
    }

    private DeveloperDTO populateDeveloper(String name, String url) {
        log.info("[parser] Developer name: " + name);
        Optional<DeveloperDTO> developerOpt = developerService.getDeveloper(name);
        DeveloperDTO developer;

        if(developerOpt.isPresent()) {
            log.info("[parser] Developer already in database");
            developer = developerOpt.get();
        } else {
            log.info("[parser] Create developer");
            developer = new DeveloperDTO();
            developer.setName(name);
            developer.setSeoName(HTMLTools.translitAndTrim(name));
            developer.setEnabled(true);
            developer = developerService.save(developer);
        }

        try {
            Document doc = Jsoup.connect(url).get();

            // Описание
            Elements desc = doc.select("#BuildDescription");

            Whitelist whitelist = new Whitelist();
            whitelist.addTags("p", "h4");

            developer.setDescription(Jsoup.clean(desc.html(), whitelist));

            // Данные
            Elements trs = doc.select("#BuildData tr");

            for (Element tr : trs) {
                Elements tds = tr.select("td");

                Element key = tds.get(0);
                Element val = tds.get(1);

                switch (key.text()) {
                    case "Адрес офиса:":
                        Pattern patterna = Pattern.compile(" \\(");
                        String[] stra = patterna.split(val.text());
                        developer.setAddress(stra[0]);

                        log.info("[parser] Developer address: {}", developer.getAddress());
                        break;

                    case "Телефон:":
                        developer.setPhone(val.text());
                        log.info("[parser] Developer phone: {}", developer.getPhone());
                        break;

                    case "E-mail:":
                        developer.setEmail(val.text());
                        log.info("[parser] Developer email: {}", developer.getEmail());
                        break;

                    case "Официальный сайт:":
                        developer.setSite(val.text());
                        log.info("[parser] Developer site: {}", developer.getSite());
                        break;

                    case "Дата создания:":
                        developer.setFoundAt(val.text());
                        log.info("[parser] Developer found at: {}", developer.getFoundAt());
                        break;
                }

                Elements logo = doc.select("#BuildImgBig_c");

                if (logo == null) {
                    log.error("[parser] No developer logo!");
                    return developer;
                }

                String imgsrc = logo.attr("src");

                if (StringUtils.isEmpty(developer.getLogo())) {
                    BufferedImage originalImage;
                    try {
                        if (imgsrc.contains("http://") || imgsrc.contains("https://")) {
                            log.info("[parser] Developer logo URL: {}", imgsrc);
                            originalImage = ImageIO.read(new URL(imgsrc));
                        } else if (imgsrc.contains("base64")) {
                            log.info("[parser] Developer logo URL (base64): {}", imgsrc);
                            originalImage = decodeBase64Image(imgsrc);
                        } else {
                            log.info("[parser] No developer logo image!");
                            return developerService.save(developer);
                        }
                    } catch (Exception e) {
                        log.error("[parser] Developer logo error", e);
                        return developerService.save(developer);
                    }

                    int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_RGB : originalImage.getType();

                    BufferedImage resizeImageJpg = resizeImage(originalImage, type);
                    ImageIO.write(resizeImageJpg, "jpg", new File("html/static/images/developers/developer-" + developer.getId() + ".jpg"));

                    developer.setLogo("developer-" + developer.getId() + ".jpg");
                }
            }
        }
        catch (IOException e) {
            log.error("[parser] Developer error", e);
            return null;
        }

        log.info("[parser] Saving developer into database");

        return developerService.save(developer);
    }

    private LocationDTO createLocation(String name) {
        log.info("[parser] Create location: {}", name);

        LocationDTO location = LocationDTO.builder()
                .name(name)
                .seoName(HTMLTools.translitAndTrim(name))
                .description("Описание пока не предоставлено")
                .build();

        return locationService.saveLocation(location);
    }

    private BufferedImage resizeImage(BufferedImage originalImage, int type)
    {
        int h = originalImage.getHeight();
        int w = originalImage.getWidth();

        if(h > 220)
            h = 220;
        else
            return genImage(originalImage);

        if(w > 270)
            w = 270;
        else
            return genImage(originalImage);

        BufferedImage resizedImage = new BufferedImage(w, h, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, 270, 220, null);
        g.dispose();

        return resizedImage;
    }

    private static BufferedImage decodeBase64Image(String uri) {
        String[] parts = uri.split(",");
        byte[] decodedString = Base64.decodeBase64(parts[1]);
        ByteArrayInputStream is = new ByteArrayInputStream(decodedString);

        try {
            return ImageIO.read(is);
        } catch (IOException e) {
            log.error("Error while decoding base64 image: " + e.getMessage());
            return null;
        }
    }

    private static BufferedImage genImage(BufferedImage srcImage) {

        BufferedImage newImage = new BufferedImage(270, 220, BufferedImage.TYPE_INT_RGB);
        Graphics graphics = newImage.getGraphics();
        graphics.setColor(Color.WHITE);
        graphics.fillRect(0, 0, 270, 220);

        graphics.drawImage(
                srcImage,
                (135 - srcImage.getWidth() / 2),
                (110 - srcImage.getHeight() / 2),
                srcImage.getWidth() > 270 ? 270 : srcImage.getWidth(),
                srcImage.getHeight(),
                Color.WHITE,
                null);

        graphics.dispose();

        return newImage;
    }
}
