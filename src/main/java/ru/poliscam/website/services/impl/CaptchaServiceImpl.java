package ru.poliscam.website.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestOperations;
import ru.poliscam.website.models.web.GoogleResponse;
import ru.poliscam.website.services.CaptchaService;

import java.net.URI;
import java.util.regex.Pattern;

@Service
public class CaptchaServiceImpl implements CaptchaService {

    private static Pattern RESPONSE_PATTERN = Pattern.compile("[A-Za-z0-9_-]+");
    @Value("${google.recaptcha.key.secret}")
    private String secretKey;
    @Autowired
    private RestOperations restTemplate;

    @Override
    public boolean isCaptchaValid(String response, String remoteAddr) {
        if (!responseSanityCheck(response)) {
            return false;
        }

        URI verifyUri = URI.create(String.format(
                "https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s",
                secretKey, response, remoteAddr));

        GoogleResponse googleResponse = restTemplate.getForObject(verifyUri, GoogleResponse.class);

        if (googleResponse != null && !googleResponse.isSuccess()) {
            return false;
        }

        return true;
    }

    private boolean responseSanityCheck(String response) {
        return StringUtils.hasLength(response) && RESPONSE_PATTERN.matcher(response).matches();
    }
}