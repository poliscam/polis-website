package ru.poliscam.website.services.impl;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.models.sql.Developer;
import ru.poliscam.website.repositories.DeveloperDAO;
import ru.poliscam.website.services.DeveloperService;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

@Service
public class DeveloperServiceImpl implements DeveloperService {
    @Autowired
    private DeveloperDAO developerDAO;
    @Autowired
    private ModelMapper mapper;
    @Autowired
    private BackendCache<Long, DeveloperDTO> cache;

    @PostConstruct
    public void init() {
        Converter<Developer, DeveloperDTO> converter = context -> {
            Set<DeveloperDTO.ComplexShortDTO> complexes = new TreeSet<>();

            if (!CollectionUtils.isEmpty(context.getSource().getComplexes())) {
                context.getSource().getComplexes().forEach(complex -> complexes.add(
                        DeveloperDTO.ComplexShortDTO.builder()
                                .id(complex.getId())
                                .name(complex.getName())
                                .enabled(complex.getEnabled())
                                .archive(complex.getArchive())
                                .build()
                ));
            }

            context.getDestination().setComplexes(complexes);
            return context.getDestination();
        };

        mapper.createTypeMap(Developer.class, DeveloperDTO.class).setPreConverter(converter);
    }

    @Override
    @SuppressWarnings("Duplicates")
    public Optional<DeveloperDTO> getDeveloper(Long id) {
        Optional<DeveloperDTO> developerFromCache = cache.get(id);

        if (developerFromCache.isPresent()) {
            return developerFromCache;
        } else {
            Optional<Developer> developerFromDb = developerDAO.findById(id);

            if (!developerFromDb.isPresent()) {
                return Optional.empty();
            } else {
                Developer developer = developerFromDb.get();
                DeveloperDTO developerDTO = mapper.map(developer, DeveloperDTO.class);
                cache.put(developerDTO.getId(), developerDTO);

                return Optional.of(developerDTO);
            }
        }
    }

    @Override
    public Optional<DeveloperDTO> getDeveloper(String name) {
            Optional<Developer> developerFromDb = developerDAO.findByName(name);

            if (!developerFromDb.isPresent()) {
                return Optional.empty();
            } else {
                Developer developer = developerFromDb.get();
                DeveloperDTO developerDTO = mapper.map(developer, DeveloperDTO.class);
                cache.put(developerDTO.getId(), developerDTO);

                return Optional.of(developerDTO);
            }
    }

    @Override
    public List<DeveloperDTO> getDevelopers() {
        List<DeveloperDTO> developerDTOList = new ArrayList<>();
        developerDAO.findByEnabledTrue().forEach(developer -> {
            DeveloperDTO dto = mapper.map(developer, DeveloperDTO.class);
            cache.put(developer.getId(), dto);
            developerDTOList.add(dto);
        });
        return developerDTOList;
    }

    @Override
    public List<DeveloperDTO> getDevelopers(int page, int size) {
        List<DeveloperDTO> developerDTOList = new ArrayList<>();
        developerDAO.findAllByEnabledTrue(PageRequest.of(page, size)).forEach(developer -> {
            DeveloperDTO dto = mapper.map(developer, DeveloperDTO.class);
            cache.put(developer.getId(), dto);
            developerDTOList.add(dto);
        });
        return developerDTOList;
    }

    @Override
    public DeveloperDTO save(DeveloperDTO developer) {
        Developer devDb = developerDAO.save(mapper.map(developer, Developer.class));
        developer = mapper.map(devDb, DeveloperDTO.class);
        cache.put(developer.getId(), developer);
        return developer;
    }
}
