package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.models.sql.Task;
import ru.poliscam.website.repositories.CameraDAO;
import ru.poliscam.website.repositories.TaskDAO;
import ru.poliscam.website.services.CameraService;
import ru.poliscam.website.services.ComplexService;

import java.util.*;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Service
@Log4j2
public class CameraServiceImpl implements CameraService {
    @Autowired
    private CameraDAO cameraDAO;

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BackendCache<Long, CameraDTO> cache;

    @Autowired
    private BackendCache<Long, ComplexDTO> complexCache;

    @Autowired
    private ComplexService complexService;

    @Override
    @SuppressWarnings("Duplicates")
    public CameraDTO getCamera(Long id) {
        Optional<CameraDTO> cameraFromCache = cache.get(id);

        if (cameraFromCache.isPresent()) {
            return cameraFromCache.get();
        } else {
            Optional<Camera> cameraFromDb = cameraDAO.findById(id);

            if (!cameraFromDb.isPresent()) {
                throw new IllegalArgumentException("Camera not found!");
            } else {
                Camera camera = cameraFromDb.get();
                CameraDTO cameraDTO = mapper.map(camera, CameraDTO.class);
                cache.put(cameraDTO.getId(), cameraDTO);

                return cameraDTO;
            }
        }
    }

    @Override
    public CameraDTO saveCamera(CameraDTO cameraDTO) {
        Camera camera = mapper.map(cameraDTO, Camera.class);
        Set<Task> tasks = new HashSet<>();

        cameraDTO.getTasks().forEach(task -> {
            Optional<Task> taskDb = taskDAO.findById(task.getId());
            taskDb.ifPresent(tasks::add);
        });

        camera.setTasks(tasks);

        camera = cameraDAO.save(camera);
        cameraDTO = mapper.map(camera, CameraDTO.class);

        cache.put(cameraDTO.getId(), cameraDTO);
        complexCache.evict(cameraDTO.getComplexId());

        return cameraDTO;
    }

    @Override
    public void deleteCamera(Long id) {
        cameraDAO.deleteById(id);
        cache.evict(id);
    }

    @Override
    public Set<CameraDTO> getCamerasByComplex(Long complexId) {
        Set<CameraDTO> cameras = new HashSet<>();
        Optional<ComplexDTO> complexOpt = complexService.getComplex(complexId);

        if(complexOpt.isPresent()) {
            complexOpt.get().getCameras().forEach(camera -> cameras.add(getCamera(camera.getId())));
            return cameras;
        } else {
            log.error("Complex not found!");
            return Collections.emptySet();
        }
    }

    @Override
    public Set<CameraDTO> getCamerasByServer(Long serverId) {
        Set<CameraDTO> cameras = new HashSet<>();
        List<Camera> cameraDb = cameraDAO.findByServerId(serverId);
        cameraDb.forEach(camera -> cameras.add(getCamera(camera.getId())));
        return cameras;
    }
}
