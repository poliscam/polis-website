package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.CameraForWorkerDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.messaging.ControlMessage;
import ru.poliscam.website.services.WorkerService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Nikolay Viguro, 01.11.18
 */

@Service
@Log4j2
public class WorkerServiceImpl implements WorkerService {
    @Autowired
    private AmqpTemplate template;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BackendCache<Long, WorkerInformationDTO> cache;

    @Override
    public void sendReload(Long id) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RELOAD_WORKER)
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendRestart(Long id) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RESTART)
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendUpdate(Long id) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.UPDATE)
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendCreateDailyVideo(Long id, CameraDTO cameraDTO) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RUN_TASK)
                                                .camera(modelMapper.map(cameraDTO, CameraForWorkerDTO.class))
                                                .clazz("ru.poliscam.worker.jobs.quartz.serial.CreateDailyVideoSerialJob")
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendCreateAlltimeVideo(Long id, CameraDTO cameraDTO) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RUN_TASK)
                                                .camera(modelMapper.map(cameraDTO, CameraForWorkerDTO.class))
                                                .clazz("ru.poliscam.worker.jobs.quartz.serial.CreateAlltimeVideoSerialJob")
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendCreateWeeklyVideo(Long id, CameraDTO cameraDTO) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RUN_TASK)
                                                .camera(modelMapper.map(cameraDTO, CameraForWorkerDTO.class))
                                                .clazz("ru.poliscam.worker.jobs.quartz.serial.CreateWeeklyVideoSerialJob")
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public void sendUploadDailyVideo(Long id, CameraDTO cameraDTO) {
        template.convertAndSend(RabbitMqConfig.QUEUE_CONTROL,
                ControlMessage.builder()
                        .identifier(id)
                        .commands(
                                Collections.singletonList(
                                        ControlMessage.Command.builder()
                                                .type(ControlMessage.Command.CommandType.RUN_TASK)
                                                .camera(modelMapper.map(cameraDTO, CameraForWorkerDTO.class))
                                                .clazz("ru.poliscam.worker.jobs.quartz.serial.UploadDailyVideoSerialJob")
                                                .build()
                                )
                        )
                        .build());
    }

    @Override
    public List<WorkerInformationDTO> getWorkers() {
        return new ArrayList<>(cache.getAll().values());
    }
}
