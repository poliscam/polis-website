package ru.poliscam.website.services.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.poliscam.website.enums.AccountType;
import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.models.sql.Account;
import ru.poliscam.website.repositories.AccountDAO;
import ru.poliscam.website.services.AccountService;

import java.util.Collections;

/**
 * @author Nikolay Viguro, 11.10.18
 */

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public AccountDTO findAccount(String s) {
        Account account = accountDAO.findByEmail(s);

        if (account == null) {
            return null;
        }

        return mapper.map(account, AccountDTO.class);
    }

    @Override
    public Account saveAccount(AccountDTO accountDTO) {
        Account account = mapper.map(accountDTO, Account.class);

        account.setAuthority(Collections.singleton(AccountType.REGULAR_USER));
        account.setPassword(passwordEncoder.encode(account.getPassword()));

        return accountDAO.save(account);
    }
}
