package ru.poliscam.website.services.impl;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.SubscriptionDTO;
import ru.poliscam.website.models.sql.Subscription;
import ru.poliscam.website.repositories.SubscriptionDAO;
import ru.poliscam.website.services.SubscriptionService;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
@Log4j2
public class SubscriptionServiceImpl implements SubscriptionService {
    @Autowired
    private SubscriptionDAO subscriptionDAO;

    @Autowired
    private BackendCache<String, SubscriptionDTO> cache;

    @Autowired
    private InitClients initClients;

    @Autowired
    private ModelMapper modelMapper;

    @Value("classpath:poliscam-push-firebase.json")
    private Resource resourceFile;

    @PostConstruct
    public void init() {
        try (InputStream serviceAccount = resourceFile.getInputStream()) {
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount)).build();
            FirebaseApp.initializeApp(options);
        }
        catch (IOException e) {
            log.error("init fcm", e);
        }

        List<List<Subscription>> subSets = Lists.partition(subscriptionDAO.findAll(), 1000);
        subSets.forEach(s -> initClients.run(s));
    }

    @Override
    public void addSubscription(String id) {
        Optional<SubscriptionDTO> subOpt = cache.get(id);
        if(subOpt.isPresent()) {
            return;
        }

        log.info("Adding subscription: {}", id);
        Subscription subscription = subscriptionDAO.save(new Subscription(new Date(), id));
        cache.put(id, modelMapper.map(subscription, SubscriptionDTO.class));

        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance()
                    .subscribeToTopicAsync(Collections.singletonList(id), "chunk").get();
            log.info(response.getSuccessCount() + " tokens were subscribed successfully");
        }
        catch (InterruptedException | ExecutionException e) {
            log.error("subscribe", e);
        }
    }

    @Override
    public void send(String text) {
        Message message = Message.builder().setTopic("chunk")
                .setWebpushConfig(WebpushConfig.builder().putHeader("ttl", "300")
                        .setNotification(new WebpushNotification(
                                "PolisCam.Ru",
                                text,
                                "/images/apple-touch-icon-114x114.png"))

                        .build())
                .build();

        String response = null;
        try {
            response = FirebaseMessaging.getInstance().sendAsync(message).get();
        } catch (InterruptedException e) {
            log.error("FCM send interrupted!");
        } catch (ExecutionException e) {
            log.error("FCM send execution exception", e);
        }
        log.info("Sent message: {}", response);
    }

    @Component
    public class InitClients {
        @Async
        public void run(List < Subscription > all) {
        if (CollectionUtils.isEmpty(all)) {
            log.info("No clients for subscription!");
            return;
        }

        all.forEach(s -> cache.put(s.getSubscriptionId(), modelMapper.map(s, SubscriptionDTO.class)));

        List<String> clients = all.stream().map(Subscription::getSubscriptionId).collect(Collectors.toList());
        try {
            TopicManagementResponse response = FirebaseMessaging.getInstance()
                    .subscribeToTopicAsync(clients, "chunk").get();
            log.info(response.getSuccessCount() + " tokens were subscribed successfully");
        } catch (InterruptedException | ExecutionException e) {
            log.error("init subscribe", e);
        }
    }
    }
}
