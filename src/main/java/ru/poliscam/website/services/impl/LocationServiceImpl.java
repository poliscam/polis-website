package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.poliscam.website.models.dto.LocationDTO;
import ru.poliscam.website.models.sql.Location;
import ru.poliscam.website.repositories.LocationDAO;
import ru.poliscam.website.services.LocationService;

import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class LocationServiceImpl implements LocationService {
    @Autowired
    private LocationDAO locationDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public Optional<LocationDTO> getLocation(Long id) {
        Optional<Location> location = locationDAO.findById(id);
        return location.map(location1 -> modelMapper.map(location1, LocationDTO.class));
    }

    @Override
    public Optional<LocationDTO> getLocation(String name) {
        Optional<Location> location = locationDAO.findByName(name);
        return location.map(location1 -> modelMapper.map(location1, LocationDTO.class));
    }

    @Override
    public Collection<LocationDTO> getAllLocations() {
        return locationDAO.findAll().stream()
                .map(l -> modelMapper.map(l, LocationDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public LocationDTO saveLocation(LocationDTO location) {
        Location locationToSave = modelMapper.map(location, Location.class);
        locationToSave = locationDAO.save(locationToSave);
        return modelMapper.map(locationToSave, LocationDTO.class);
    }
}
