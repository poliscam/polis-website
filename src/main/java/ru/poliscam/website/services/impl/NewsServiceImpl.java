package ru.poliscam.website.services.impl;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import ru.poliscam.website.models.dto.NewsDTO;
import ru.poliscam.website.models.sql.News;
import ru.poliscam.website.repositories.NewsDAO;
import ru.poliscam.website.services.NewsService;
import ru.poliscam.website.services.SubscriptionService;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDAO newsDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private SubscriptionService subscriptionService;

    @Override
    public List<NewsDTO> getNews(PageRequest page) {
        return newsDAO.findAll(page).stream()
                .map(news -> modelMapper.map(news, NewsDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    public Optional<NewsDTO> getLastNews() {
        return newsDAO.findAll(Sort.by(Sort.Direction.DESC, "id"))
                .stream()
                .map(n -> modelMapper.map(n, NewsDTO.class))
                .findFirst();
    }

    @Override
    public Optional<NewsDTO> addNews(NewsDTO news) {
        String text = news.getText();
        news.setText(news.getText().replaceAll("\n", "<br>"));

        if(StringUtils.isEmpty(text)) {
            log.error("Empty news text. Skipped");
            return Optional.empty();
        }

        if(text.length() > 300) {
            text = text.substring(0, 299) + "...";
        }

        news.setPostedAt(new Date());
        News newsDb = newsDAO.save(modelMapper.map(news, News.class));
        subscriptionService.send(text);

        return Optional.of(modelMapper.map(newsDb, NewsDTO.class));
    }

    @Override
    public Optional<NewsDTO> getNews(Long id) {
        return newsDAO.findById(id).map(news -> modelMapper.map(news, NewsDTO.class));
    }
}
