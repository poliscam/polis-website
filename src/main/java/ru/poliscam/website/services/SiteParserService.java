package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.ComplexDTO;

import java.util.Optional;

public interface SiteParserService {
    Optional<ComplexDTO> getComplex(String url);
}
