package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;

import java.util.List;

/**
 * @author Nikolay Viguro 01.11.18
 */
public interface WorkerService {
    void sendReload(Long id);
    void sendRestart(Long id);
    void sendUpdate(Long id);

    void sendCreateDailyVideo(Long id, CameraDTO cameraDTO);
    void sendCreateAlltimeVideo(Long id, CameraDTO cameraDTO);
    void sendCreateWeeklyVideo(Long id, CameraDTO cameraDTO);
    void sendUploadDailyVideo(Long id, CameraDTO cameraDTO);

    List<WorkerInformationDTO> getWorkers();
}
