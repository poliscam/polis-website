package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.CameraDTO;

import java.util.Set;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface CameraService {
    CameraDTO getCamera(Long id);

    CameraDTO saveCamera(CameraDTO cameraDTO);

    void deleteCamera(Long id);

    Set<CameraDTO> getCamerasByComplex(Long complexId);

    Set<CameraDTO> getCamerasByServer(Long serverId);
}
