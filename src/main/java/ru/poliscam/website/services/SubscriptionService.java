package ru.poliscam.website.services;

/**
 * @author Nikolay Viguro, 11.19.18
 */

public interface SubscriptionService {
    void addSubscription(String id);
    void send(String text);
}
