package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.SearchedObjectDTO;

import java.util.List;

public interface SearchService {
    List<SearchedObjectDTO> getResults(String q);
    void reload();
}
