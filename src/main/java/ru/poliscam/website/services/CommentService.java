package ru.poliscam.website.services;

import ru.poliscam.website.models.Page;
import ru.poliscam.website.models.dto.CommentDTO;

import java.util.Optional;
import java.util.Set;

/**
 * @author Nikolay Viguro, 07.05.18
 */

public interface CommentService {
    void addComment(CommentDTO comment);
    void deleteComment(Long cid, Long id);

    Optional<CommentDTO> getComment(Long id);
    CommentDTO saveComment(CommentDTO toSave);

    Long getCommentsCount(Long id);

    Set<CommentDTO> getCommentByComplex(Long id, Page page);
}
