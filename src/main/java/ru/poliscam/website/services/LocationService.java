package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.LocationDTO;

import java.util.Collection;
import java.util.Optional;

/**
 * @author Nikolay Viguro, 11.11.18
 */

public interface LocationService {
    Optional<LocationDTO> getLocation(Long id);
    Optional<LocationDTO> getLocation(String name);

    Collection<LocationDTO> getAllLocations();

    LocationDTO saveLocation(LocationDTO location);
}
