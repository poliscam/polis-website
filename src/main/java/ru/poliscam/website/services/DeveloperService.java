package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.DeveloperDTO;

import java.util.List;
import java.util.Optional;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface DeveloperService {
    Optional<DeveloperDTO> getDeveloper(Long id);
    Optional<DeveloperDTO> getDeveloper(String name);

    List<DeveloperDTO> getDevelopers();

    List<DeveloperDTO> getDevelopers(int page, int size);

    DeveloperDTO save(DeveloperDTO developer);
}
