package ru.poliscam.website.services;

/**
 * @author Nikolay Viguro, 25.05.18
 */
public interface CaptchaService {
    boolean isCaptchaValid(String response, String remoteAddr);
}
