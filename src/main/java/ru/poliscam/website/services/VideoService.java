package ru.poliscam.website.services;

import ru.poliscam.website.enums.VideoType;
import ru.poliscam.website.models.dto.VideoDTO;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

public interface VideoService {
    List<VideoDTO> getVideos(Long id, Date date, VideoType type);
}
