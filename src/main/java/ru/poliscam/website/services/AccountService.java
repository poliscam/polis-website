package ru.poliscam.website.services;

import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.models.sql.Account;

/**
 * @author Nikolay Viguro 11.10.18
 */
public interface AccountService {
    AccountDTO findAccount(String email);

    Account saveAccount(AccountDTO accountDTO);
}
