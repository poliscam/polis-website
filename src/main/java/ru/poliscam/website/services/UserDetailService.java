package ru.poliscam.website.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.models.sql.Account;
import ru.poliscam.website.repositories.AccountDAO;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

@Service
@Qualifier("polisUserDetailsService")
public class UserDetailService implements UserDetailsService {
    @Autowired
    private AccountDAO accountDAO;

    @Autowired
    private ModelMapper mapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Account account = accountDAO.findByEmail(s);

        if (account == null) {
            throw new UsernameNotFoundException("user not found!");
        }

        return mapper.map(account, AccountDTO.class);
    }
}
