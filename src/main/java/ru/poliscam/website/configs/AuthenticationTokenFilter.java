package ru.poliscam.website.configs;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;
import ru.poliscam.website.services.CaptchaService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Nikolay Viguro at 30.03.17
 */

@Log4j2
public class AuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    private CaptchaService captchaService;

    public AuthenticationTokenFilter() {
        super();
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        String captcha = request.getParameter("g-recaptcha-response");
        String remoteAddr = request.getHeader("X-FORWARDED-FOR");
        if (remoteAddr == null || "".equals(remoteAddr)) {
            remoteAddr = request.getRemoteAddr();
        }

        if (!StringUtils.isEmpty(captcha)) {
            if (captchaService.isCaptchaValid(captcha, remoteAddr)) {
                log.debug("ReCaptcha answer is valid, attempt authentication");
                try {
                    return super.attemptAuthentication(request, response);
                } catch (AuthenticationException e) {
                    log.error("Auth fail for {}", remoteAddr);
                    error(response, "Auth error");
                    return null;
                }
            } else {
                log.error("ReCaptcha fail for {}", remoteAddr);
                error(response, "ReCaptcha fail!");
                return null;
            }
        } else {
            log.error("No ReCaptcha in request!");
            error(response, "ReCaptcha is empty!");
            return null;
        }
    }

    private void error(HttpServletResponse response, String errorMsg) {
        try {
            response.sendRedirect("/login?error");
        }
        catch (IOException e) {
            throw new AuthenticationServiceException("Auth failed: " + errorMsg);
        }
    }
}
