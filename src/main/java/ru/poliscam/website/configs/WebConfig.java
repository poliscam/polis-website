package ru.poliscam.website.configs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.Formatter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.poliscam.website.models.dto.TaskDTO;

import java.text.ParseException;
import java.util.Locale;

/**
 * @author nix (08.05.2018)
 */
@Configuration
@EnableWebMvc
@ComponentScan
public class WebConfig extends WebMvcAutoConfiguration {
    @Autowired
    private FormatterRegistry formatterRegistry;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public FormatterRegistry formatterRegistry() {
        formatterRegistry.addFormatterForFieldType(TaskDTO.class, new TaskConverter());
        return formatterRegistry;
    }

    public static class TaskConverter implements Formatter<TaskDTO> {
        @Override
        public TaskDTO parse(String s, Locale locale) throws ParseException {
            TaskDTO taskDTO = new TaskDTO();
            taskDTO.setId(Long.valueOf(s));
            return taskDTO;
        }

        @Override
        public String print(TaskDTO taskDTO, Locale locale) {
            return taskDTO.getId().toString();
        }
    }
}