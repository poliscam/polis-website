package ru.poliscam.website.configs;

import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.node.NodeClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.node.InternalSettingsPreparer;
import org.elasticsearch.node.Node;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.transport.Netty4Plugin;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableElasticsearchRepositories(basePackages = "ru.poliscam.website.models.elastic")
@ComponentScan(basePackages = {"ru.poliscam.website.models.elastic"})
@Slf4j
public class ElasticsearchConfig implements DisposableBean {

    private NodeClient client;

    @Value("${elasticsearch.path.home:.}")
    private String esHomePath;

    @Value("${elasticsearch.path.config:config}")
    private String esConfigPath;

    @Bean
    public ElasticsearchTemplate elasticsearchTemplate() {
        return new ElasticsearchTemplate(esClient());
    }

    @Bean
    public Client esClient() {
        try {
            log.info("Loading Elasticsearch client configuration");

            Settings settings = Settings.builder()
                    .put("path.home", esHomePath)
                    .put("path.conf", esConfigPath)
                    .build();

            Node node = new PluginConfigurableNode(settings, Collections.singletonList(Netty4Plugin.class)).start();
            this.client = (NodeClient) node.client();

            log.info("Elasticsearch client created");

            return this.client;
        } catch (Exception ex) {
            throw new IllegalStateException(ex);
        }
    }

    @Override
    public void destroy() {
        if (this.client != null) {
            try {
                log.info("Closing Elasticsearch client");

                if (this.client != null) {
                    this.client.close();
                }
            } catch (final Exception ex) {
                log.error("Error closing Elasticsearch client: ", ex);
            }
        }
    }

    public class PluginConfigurableNode extends Node {
        public PluginConfigurableNode(Settings settings, Collection<Class<? extends Plugin>> classpathPlugins) {
            super(InternalSettingsPreparer.prepareEnvironment(settings, null), classpathPlugins);
        }
    }
}
