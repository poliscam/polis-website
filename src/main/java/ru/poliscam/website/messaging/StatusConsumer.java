package ru.poliscam.website.messaging;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.messaging.StatusMessage;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
@Log4j2
public class StatusConsumer {
    @Autowired
    private BackendCache<Long, CameraDTO> cameras;

    @RabbitListener(queues = RabbitMqConfig.QUEUE_STATUS)
    public void processMessage(StatusMessage message) {
        if (message != null && message.getCameraId() != null) {
            Optional<CameraDTO> cameraOpt = cameras.get(message.getCameraId());

            if (cameraOpt.isPresent() && cameraOpt.get().getEnabled()) {
                CameraDTO camera = cameraOpt.get();

                switch (message.getStatus()) {
                    case IMAGE_FETCHED: {
                        camera.setLastSuccessImageFetch(LocalDateTime.now());
                        break;
                    }
                    case IMAGE_NOT_FETCHED: {
                        camera.setLastFailedImageFetch(LocalDateTime.now());
                        break;
                    }
                    default: {
                        log.warn("Unknown status in StatusMessage come: {}", message.getStatus());
                    }
                }
                cameras.put(camera.getId(), camera);
            } else {
                log.warn("Message come from worker with identifier #{}, but camera #{} doesn't exist or disabled in cache",
                        message.getWorker(), message.getCameraId());
            }
        } else {
            log.error("No content come in heartbeat message!");
        }
    }
}
