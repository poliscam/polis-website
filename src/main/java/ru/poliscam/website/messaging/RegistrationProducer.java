package ru.poliscam.website.messaging;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.messaging.RegistrationMessage;

import java.time.LocalDateTime;

@Component
@Log4j2
public class RegistrationProducer {
    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    @RabbitListener(queues = RabbitMqConfig.QUEUE_REGISTRATION)
    public String processMessage(RegistrationMessage message) {
        if (message != null && message.getIdentifier() != null) {
            if (workers.get(message.getIdentifier()).isPresent() && workers.get(message.getIdentifier()).get().getEnabled()) {
                WorkerInformationDTO worker = workers.get(message.getIdentifier()).get();

                if (message.getVersion() != null) {
                    worker.setVersion(message.getVersion());
                }

                if(!StringUtils.isEmpty(message.getIp())) {
                    worker.setIp(message.getIp());
                }

                worker.setLastSeen(LocalDateTime.now());
                workers.put(message.getIdentifier(), worker);

                if (!StringUtils.isEmpty(worker.getAdditionalConfig())) {
                    return worker.getAdditionalConfig();
                } else {
                    log.error("Configuration is null for worker #{}", worker.getId());
                }
            } else {
                log.warn("Message come from worker with identifier #{}, but it doesn't exist or disabled in cache",
                        message.getIdentifier());
            }
        } else {
            log.error("No content come in registration message!");
        }

        return null;
    }
}
