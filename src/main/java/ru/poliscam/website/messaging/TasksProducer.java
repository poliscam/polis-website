package ru.poliscam.website.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.CameraForWorkerDTO;
import ru.poliscam.website.models.dto.TaskDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.messaging.IdentifierMessage;
import ru.poliscam.website.models.messaging.TasksMessage;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.models.sql.Task;
import ru.poliscam.website.repositories.CameraDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Log4j2
public class TasksProducer {
    @Autowired
    private BackendCache<Long, CameraDTO> camerasCache;

    @Autowired
    private CameraDAO cameraDAO;

    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @RabbitListener(queues = RabbitMqConfig.QUEUE_TASKS)
    public String processMessage(IdentifierMessage message) {
        if (message != null && message.getIdentifier() != null) {
            if (workers.get(message.getIdentifier()).isPresent() && workers.get(message.getIdentifier()).get().getEnabled()) {
                List<CameraForWorkerDTO> camerasForWorker = camerasCache.getAll().values().stream()
                        .filter(c -> !c.getArchive())
                        .filter(CameraDTO::getEnabled)
                        .filter(c -> c.getServer() != null && c.getServer().getId().equals(message.getIdentifier()))
                        .map(c -> modelMapper.map(c, CameraForWorkerDTO.class))
                        .collect(Collectors.toList());

                camerasForWorker.forEach(c -> {
                    c.setTasks(new ArrayList<>());
                    Camera camera = cameraDAO.findById(c.getId()).orElse(new Camera());
                    camera.getTasks().stream().filter(Task::isEnabled).forEach(t -> {
                        c.getTasks().add(modelMapper.map(t, TaskDTO.class));
                    });
                });

                try {
                    return objectMapper.writeValueAsString(new TasksMessage(camerasForWorker));
                } catch (JsonProcessingException e) {
                    log.error("Can't convert task message", e);
                }
            } else {
                log.warn("Message come from worker with identifier #{}, but it doesn't exist or disabled in cache",
                        message.getIdentifier());
            }
        }
        return null;
    }
}
