package ru.poliscam.website.messaging;

import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.messaging.HeartbeatMessage;

import java.time.LocalDateTime;

@Component
@Log4j2
public class HeartbeatConsumer {
    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    @RabbitListener(queues = RabbitMqConfig.QUEUE_HEARTBEAT)
    public void processMessage(HeartbeatMessage message) {
        if (message != null && message.getIdentifier() != null) {
            if (workers.get(message.getIdentifier()).isPresent() && workers.get(message.getIdentifier()).get().getEnabled()) {
                WorkerInformationDTO worker = workers.get(message.getIdentifier()).get();

                worker.setVersion(message.getVersion());
                worker.setLastSeen(LocalDateTime.now());

                workers.put(worker.getId(), worker);
            } else {
                log.warn("Message come from worker with identifier #{}, but it doesn't exist or disabled in cache",
                        message.getIdentifier());
            }
        } else {
            log.error("No content come in heartbeat message!");
        }
    }
}
