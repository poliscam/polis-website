package ru.poliscam.website.messaging;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.configs.RabbitMqConfig;
import ru.poliscam.website.enums.VideoType;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.messaging.VideoUploaderMessage;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.models.sql.Video;
import ru.poliscam.website.repositories.CameraDAO;
import ru.poliscam.website.repositories.VideoDAO;

@Component
@Log4j2
public class VideoUploaderConsumer {
    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    @Autowired
    private BackendCache<Long, CameraDTO> camerasCache;

    @Autowired
    private CameraDAO cameraDAO;

    @Autowired
    private VideoDAO videoDAO;

    @Autowired
    private ModelMapper modelMapper;

    @RabbitListener(queues = RabbitMqConfig.QUEUE_VIDEO)
    public void processMessage(VideoUploaderMessage message) {
        if (message != null && message.getWorker() != null) {
            if (workers.get(message.getWorker()).isPresent() && workers.get(message.getWorker()).get().getEnabled()) {
                if(!CollectionUtils.isEmpty(message.getVideos())) {
                    message.getVideos().forEach(upload -> {
                        Camera camera = cameraDAO.findById(upload.getCameraId()).orElseThrow(IllegalArgumentException::new);
                        VideoType videoType = null;

                        switch (upload.getType()) {
                            case DAILY:
                                camera.setLastDailyVideo(upload.getYoutubeId());
                                videoType = VideoType.DAILY;
                                break;
                            case WEEKLY:
                                camera.setLastWeekVideo(upload.getYoutubeId());
                                videoType = VideoType.WEEKLY;
                                break;
                            case ALL_TIME:
                                camera.setLastFastVideo(upload.getYoutubeId());
                                videoType = VideoType.ALL_TIME;
                        }

                        camera = cameraDAO.save(camera);
                        camerasCache.put(camera.getId(), modelMapper.map(camera, CameraDTO.class));

                        Video video = Video.builder()
                                .added(upload.getDate())
                                .camera(
                                        cameraDAO.findById(upload.getCameraId())
                                                .orElseThrow(() -> new IllegalAccessError("No camera with id " + upload.getCameraId() + " found!")))
                                .type(videoType)
                                .youtubeID(upload.getYoutubeId())
                                .build();

                        videoDAO.save(video);
                    });
                } else {
                    log.error("No video in uploadvideo message from identifier #{}", message.getWorker());
                }
            } else {
                log.warn("Message come from worker with identifier #{}, but it doesn't exist or disabled in cache",
                        message.getWorker());
            }
        } else {
            log.error("No content come in metrics message!");
        }
    }
}
