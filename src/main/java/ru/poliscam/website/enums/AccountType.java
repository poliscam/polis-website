package ru.poliscam.website.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * @author nix (24.02.2017)
 */
public enum AccountType implements GrantedAuthority {

    BANNED, // зобанен
    REGULAR_USER, // самый обычный чувак
    MODERATOR, // на перспективу
    ADMIN; // это мы :)

    @Override
    public String getAuthority() {
        return toString();
    }
}
