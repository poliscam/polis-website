package ru.poliscam.website.enums;

/**
 * Created by nix on 23.10.2018.
 */

public enum VideoType {
    DAILY,
    WEEKLY,
    ALL_TIME
}
