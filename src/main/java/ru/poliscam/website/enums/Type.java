package ru.poliscam.website.enums;

/**
 * Created by nix on 19.03.2016.
 */
public enum Type {
    URL,
    FFMPEG,
    RTMP,
    IMAGE,
    NONE
}
