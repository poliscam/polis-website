package ru.poliscam.website.enums;

import lombok.Getter;

/**
 * @author nix (26.02.2017)
 */

@Getter
public enum BackendAPIResponseCode {
    OK(200),

    NOT_FOUND(404),

    ERROR(500),

    UNAUTHORIZED(501),
    ACCOUNT_NOT_FOUND(502),
    USER_ID_NOT_FOUND(503),

    USER_ALREADY_EXISTS(601);

    private final int code;

    BackendAPIResponseCode(int code) {
        this.code = code;
    }
}
