package ru.poliscam.website.aspects;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.poliscam.website.models.web.BackendResponse;

/**
 * @author nix on 18.09.2017.
 */

@Log4j2
@Aspect
@Order // Integer.MAX_VALUE by default, so this aspect become to be run at the end
@Component
public class RunTimeAspect {
    @Around("execution(public * ru.poliscam.website.controllers.rest..*(..))")
    public Object runTime(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();

        BackendResponse res = (BackendResponse) pjp.proceed();

        long endTime = System.currentTimeMillis();
        res.setTime((endTime - startTime));
        log.info("Method \"{}\" running time: {}ms", pjp.getSignature().getName(), (endTime - startTime));

        return res;
    }
}