package ru.poliscam.website.aspects;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.services.CameraService;

import java.util.Optional;

/**
 * @author nix on 24.05.2018.
 */

@Log4j2
@Aspect
@Component
public class CountVisitorAspect {
    @Autowired
    private CameraService cameraService;

    @Autowired
    private BackendCache<Long, Long> visitorsCache;

    @Around("execution(public * ru.poliscam.website.controllers.thymeleaf.CameraController.showDefaultCameraByComplex(..)) && args(complexId, ..)")
    public Object countDefCamera(ProceedingJoinPoint pjp, Long complexId) throws Throwable {
        Optional<Long> visits = visitorsCache.get(complexId);
        Long count = 1L;
        if (visits.isPresent()) {
            count = visits.get() + 1L;
            visitorsCache.put(complexId, count);
        } else {
            visitorsCache.put(complexId, count);
        }

        log.debug("Complex #{}, visits: {}", complexId, count);

        return pjp.proceed();
    }

    @Around("execution(public * ru.poliscam.website.controllers.thymeleaf.CameraController.*(..)) && args(id, ..) && !execution(* ru.poliscam.website.controllers.thymeleaf.CameraController.showDefaultCameraByComplex(..))")
    public Object count(ProceedingJoinPoint pjp, Long id) throws Throwable {
        CameraDTO camera = cameraService.getCamera(id);
        Long complexId = camera.getComplexId();

        Optional<Long> visits = visitorsCache.get(complexId);
        Long count = 1L;
        if (visits.isPresent()) {
            count = visits.get() + 1L;
            visitorsCache.put(complexId, count);
        } else {
            visitorsCache.put(complexId, count);
        }

        log.debug("Complex #{}, visits: {}", complexId, count);

        return pjp.proceed();
    }
}