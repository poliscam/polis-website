package ru.poliscam.website.cache.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/**
 * @author nix (26.02.2017)
 */

@Configuration
@EnableCaching
public class CacheConfig {
    public static final String COMPLEXES = "complexes";
    public static final String DEVELOPERS = "developers";
    public static final String CAMERAS = "cameras";
    public static final String VISITORS = "visitors";
    public static final String COMMENTS = "comments";
    public static final String SERVERS = "servers";
    public static final String SUBSCRIBERS = "subscribers";
}
