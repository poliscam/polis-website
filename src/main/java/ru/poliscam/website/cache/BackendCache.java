package ru.poliscam.website.cache;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (27.05.2018)
 */
public interface BackendCache<K, V> {
    V put(K key, V value);
    Optional<V> get(K key);
    void evict(K key);

    default void evictAll() {
    }

    Map<K, V> getAll();
}
