package ru.poliscam.website.cache.impl.nocache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.ComplexDTO;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class ComplexNoCacheImpl implements BackendCache<Long, ComplexDTO> {
    // будет юзаться если выключен кеш
    private final Map<Long, ComplexDTO> nocache = new ConcurrentHashMap<>();

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMPLEXES, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.COMPLEXES, key = "#key")
    public ComplexDTO put(Long key, ComplexDTO value) {
        return nocache.put(key, value);
    }

    @Override
    @Cacheable(value = CacheConfig.COMPLEXES, sync = true, key = "#key")
    public Optional<ComplexDTO> get(Long key) {
        return Optional.of(nocache.get(key));
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMPLEXES, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
        nocache.remove(key);
    }

    @Override
    public Map<Long, ComplexDTO> getAll() {
        return nocache;
    }


}
