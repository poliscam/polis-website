package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.sql.Complex;
import ru.poliscam.website.repositories.ComplexDAO;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (28.04.2017)
 */

@Component
public class ComplexCacheImpl implements BackendCache<Long, ComplexDTO> {
    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private ComplexDAO complexDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMPLEXES, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.COMPLEXES, key = "#key")
    public ComplexDTO put(Long key, ComplexDTO value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.COMPLEXES, sync = true, key = "#key")
    public Optional<ComplexDTO> get(Long key) {
        Optional<Complex> opt = complexDAO.findById(key);
        if(opt.isPresent()) {
            return Optional.ofNullable(modelMapper.map(opt.get(), ComplexDTO.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMPLEXES, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMPLEXES, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, ComplexDTO> getAll() {
        return instance.getMap(CacheConfig.COMPLEXES);
    }
}
