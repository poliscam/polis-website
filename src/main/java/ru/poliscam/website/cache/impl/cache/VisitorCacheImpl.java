package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (28.04.2017)
 */

@Component
public class VisitorCacheImpl implements BackendCache<Long, Long> {

    @Autowired
    private HazelcastInstance instance;

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.VISITORS, key = "#key")
    public Long put(Long key, Long value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.VISITORS, sync = true, key = "#key")
    public Optional<Long> get(Long key) {
        return Optional.empty();
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, Long> getAll() {
        return instance.getMap(CacheConfig.VISITORS);
    }
}
