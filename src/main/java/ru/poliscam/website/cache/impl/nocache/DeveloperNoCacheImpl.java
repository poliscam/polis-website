package ru.poliscam.website.cache.impl.nocache;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.DeveloperDTO;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class DeveloperNoCacheImpl implements BackendCache<Long, DeveloperDTO> {
    @Autowired
    private HazelcastInstance instance;

    // будет юзаться если выключен кеш
    private final Map<Long, DeveloperDTO> nocache = new ConcurrentHashMap<>();

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.DEVELOPERS, key = "#key")
    public DeveloperDTO put(Long key, DeveloperDTO value) {
        return nocache.put(key, value);
    }

    @Override
    @Cacheable(value = CacheConfig.DEVELOPERS, sync = true, key = "#key")
    public Optional<DeveloperDTO> get(Long key) {
        return Optional.of(nocache.get(key));
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
        nocache.remove(key);
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, DeveloperDTO> getAll() {
        return instance.getMap(CacheConfig.DEVELOPERS);
    }
}
