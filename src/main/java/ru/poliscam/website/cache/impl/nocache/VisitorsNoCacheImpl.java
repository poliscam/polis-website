package ru.poliscam.website.cache.impl.nocache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class VisitorsNoCacheImpl implements BackendCache<Long, Long> {
    // будет юзаться если выключен кеш
    private final Map<Long, Long> nocache = new ConcurrentHashMap<>();

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.VISITORS, key = "#key")
    public Long put(Long key, Long value) {
        return nocache.put(key, value);
    }

    @Override
    @Cacheable(value = CacheConfig.VISITORS, sync = true, key = "#key")
    public Optional<Long> get(Long key) {
        return Optional.of(nocache.get(key));
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
        nocache.remove(key);
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.VISITORS, allEntries = true)
    public void evictAll() {
        nocache.clear();
    }

    @Override
    public Map<Long, Long> getAll() {
        return nocache;
    }
}
