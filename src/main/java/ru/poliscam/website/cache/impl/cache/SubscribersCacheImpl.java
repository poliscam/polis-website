package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.SubscriptionDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.sql.Server;
import ru.poliscam.website.models.sql.Subscription;
import ru.poliscam.website.repositories.ServerDAO;
import ru.poliscam.website.repositories.SubscriptionDAO;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (20.10.2018)
 */

@Component
public class SubscribersCacheImpl implements BackendCache<String, SubscriptionDTO> {

    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private SubscriptionDAO subscriptionDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.SUBSCRIBERS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.SUBSCRIBERS, key = "#key")
    public SubscriptionDTO put(String key, SubscriptionDTO value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.SUBSCRIBERS, sync = true, key = "#key")
    public Optional<SubscriptionDTO> get(String key) {
        Optional<Subscription> opt = subscriptionDAO.findBySubscriptionId(key);
        if(opt.isPresent()) {
            return Optional.ofNullable(modelMapper.map(opt.get(), SubscriptionDTO.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.SUBSCRIBERS, beforeInvocation = true, key = "#key")
    public void evict(String key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.SUBSCRIBERS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<String, SubscriptionDTO> getAll() {
        return instance.getMap(CacheConfig.SUBSCRIBERS);
    }
}
