package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.CommentDTO;
import ru.poliscam.website.repositories.CommentDAO;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author nix (28.05.2018)
 */

@Component
public class CommentsCacheImpl implements BackendCache<Long, Set<CommentDTO>> {
    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private CommentDAO commentDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.COMMENTS, key = "#key")
    public Set<CommentDTO> put(Long key, Set<CommentDTO> value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.COMMENTS, sync = true, key = "#key")
    public Optional<Set<CommentDTO>> get(Long key) {
        return Optional.empty();
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, Set<CommentDTO>> getAll() {
        return instance.getMap(CacheConfig.COMMENTS);
    }
}
