package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.repositories.CameraDAO;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (28.04.2017)
 */

@Component
public class CameraCacheImpl implements BackendCache<Long, CameraDTO> {
    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private CameraDAO cameraDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.CAMERAS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.CAMERAS, key = "#key")
    public CameraDTO put(Long key, CameraDTO value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.CAMERAS, sync = true, key = "#key")
    public Optional<CameraDTO> get(Long key) {
        Optional<Camera> opt = cameraDAO.findById(key);
        if(opt.isPresent()) {
            return Optional.ofNullable(modelMapper.map(opt.get(), CameraDTO.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.CAMERAS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.CAMERAS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, CameraDTO> getAll() {
        return instance.getMap(CacheConfig.CAMERAS);
    }
}
