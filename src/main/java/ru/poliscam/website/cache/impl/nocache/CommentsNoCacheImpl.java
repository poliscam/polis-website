package ru.poliscam.website.cache.impl.nocache;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.CommentDTO;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author nix (28.04.2017)
 */

@Component
@Profile("nocache")
public class CommentsNoCacheImpl implements BackendCache<Long, Set<CommentDTO>> {
    // будет юзаться если выключен кеш
    private final Map<Long, Set<CommentDTO>> nocache = new ConcurrentHashMap<>();

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.COMMENTS, key = "#key")
    public Set<CommentDTO> put(Long key, Set<CommentDTO> value) {
        return nocache.put(key, value);
    }

    @Override
    @Cacheable(value = CacheConfig.COMMENTS, sync = true, key = "#key")
    public Optional<Set<CommentDTO>> get(Long key) {
        return Optional.of(nocache.get(key));
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
        nocache.remove(key);
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.COMMENTS, allEntries = true)
    public void evictAll() {
        nocache.clear();
    }

    @Override
    public Map<Long, Set<CommentDTO>> getAll() {
        return nocache;
    }
}
