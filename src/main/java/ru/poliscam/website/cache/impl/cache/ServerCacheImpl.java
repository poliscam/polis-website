package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.sql.Server;
import ru.poliscam.website.repositories.ServerDAO;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (20.10.2018)
 */

@Component
public class ServerCacheImpl implements BackendCache<Long, WorkerInformationDTO> {

    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private ServerDAO serverDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.SERVERS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.SERVERS, key = "#key")
    public WorkerInformationDTO put(Long key, WorkerInformationDTO value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.SERVERS, sync = true, key = "#key")
    public Optional<WorkerInformationDTO> get(Long key) {
        Optional<Server> opt = serverDAO.findById(key);
        if(opt.isPresent()) {
            return Optional.ofNullable(modelMapper.map(opt.get(), WorkerInformationDTO.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.SERVERS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.SERVERS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, WorkerInformationDTO> getAll() {
        return instance.getMap(CacheConfig.SERVERS);
    }
}
