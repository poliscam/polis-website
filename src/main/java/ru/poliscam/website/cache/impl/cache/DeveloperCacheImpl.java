package ru.poliscam.website.cache.impl.cache;

import com.hazelcast.core.HazelcastInstance;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.cache.config.CacheConfig;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.models.sql.Developer;
import ru.poliscam.website.repositories.DeveloperDAO;

import java.util.Map;
import java.util.Optional;

/**
 * @author nix (28.04.2017)
 */

@Component
public class DeveloperCacheImpl implements BackendCache<Long, DeveloperDTO> {
    @Autowired
    private HazelcastInstance instance;

    @Autowired
    private DeveloperDAO developerDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, beforeInvocation = true, key = "#key")
    @CachePut(cacheNames = CacheConfig.DEVELOPERS, key = "#key")
    public DeveloperDTO put(Long key, DeveloperDTO value) {
        return value;
    }

    @Override
    @Cacheable(value = CacheConfig.DEVELOPERS, sync = true, key = "#key")
    public Optional<DeveloperDTO> get(Long key) {
        Optional<Developer> opt = developerDAO.findById(key);
        if(opt.isPresent()) {
            return Optional.ofNullable(modelMapper.map(opt.get(), DeveloperDTO.class));
        } else {
            return Optional.empty();
        }
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, beforeInvocation = true, key = "#key")
    public void evict(Long key) {
    }

    @Override
    @CacheEvict(cacheNames = CacheConfig.DEVELOPERS, allEntries = true)
    public void evictAll() {
    }

    @Override
    public Map<Long, DeveloperDTO> getAll() {
        return instance.getMap(CacheConfig.DEVELOPERS);
    }
}
