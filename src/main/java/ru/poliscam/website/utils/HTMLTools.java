package ru.poliscam.website.utils;

import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.models.dto.ComplexDTO;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import static ru.poliscam.website.enums.AccountType.ADMIN;

/**
 * @author Nikolay Viguro, 24.05.18
 */

public final class HTMLTools {

    public static void fillCommonData(ComplexDTO complex, Model model, Authentication auth) {
        Whitelist whitelist = new Whitelist();
        whitelist.addTags("br");

        Set<ComplexDTO.CameraShortDTO> sorted = new TreeSet<>(complex.getCameras());
        complex.setCameras(sorted);

        String description = null;
        if (!StringUtils.isEmpty(complex.getDescription())) {
            description = Jsoup.clean(complex.getDescription(), whitelist);
        }

        String location = null;
        if (complex.getLocation() != null && !StringUtils.isEmpty(complex.getLocation().getDescription())) {
            location = Jsoup.clean(complex.getLocation().getDescription(), whitelist);
        }

        String devdescription = null;
        if (complex.getDeveloper() != null && !StringUtils.isEmpty(complex.getDeveloper().getDescription())) {
            devdescription = Jsoup.clean(complex.getDeveloper().getDescription(), whitelist);
        }

        String title = complex.getName() + " - видео строительства и веб-камеры со стройки" + (complex.getDeveloper() != null ? "застройщик " + complex.getDeveloper().getName() : "");
        String keywords = Jsoup.clean(complex.getName() + "," + (complex.getDeveloper() != null ? complex.getDeveloper().getName() + "," : "") + (complex.getLocation() != null ? "веб-камера," + complex.getLocation().getName() : "веб-камера") + "," + (!StringUtils.isEmpty(complex.getKeywords()) && !complex.getKeywords().equals("null") ? complex.getKeywords() : "запись со стройки"), whitelist);
        keywords = keywords.replaceAll("\"", "");

        fillAuth(model, auth);

        model.addAttribute("title", title);
        model.addAttribute("keywords", keywords);
        model.addAttribute("description", description != null ? description : "");
        model.addAttribute("location", location != null ? location : "");
        model.addAttribute("devdescription", devdescription != null ? devdescription : "");
    }

    public static void fillAuth(Model model, Authentication auth) {
        if (auth == null) {
            auth = new Authentication() {
                private static final long serialVersionUID = -2123832929897136567L;

                @Override
                public Collection<? extends GrantedAuthority> getAuthorities() {
                    return null;
                }

                @Override
                public Object getCredentials() {
                    return null;
                }

                @Override
                public Object getDetails() {
                    return null;
                }

                @Override
                public Object getPrincipal() {
                    return null;
                }

                @Override
                public boolean isAuthenticated() {
                    return false;
                }

                @Override
                public void setAuthenticated(boolean b) throws IllegalArgumentException {

                }

                @Override
                public String getName() {
                    return null;
                }
            };

            model.addAttribute("username", auth.getName());
            model.addAttribute("auth", auth);
            model.addAttribute("admin", false);
        } else {
            AccountDTO accountDTO = (AccountDTO) auth.getPrincipal();
            model.addAttribute("username", accountDTO.getDisplayName());
            model.addAttribute("auth", auth);
            model.addAttribute("admin", auth.getAuthorities().contains(ADMIN));
        }
    }

    public static String translitAndTrim(String s)
    {
        String regexp = s.replaceAll("\"", "");
        regexp = regexp.replaceAll("ООО\\s", "");
        regexp = regexp.replaceAll("\\(", "");
        regexp = regexp.replaceAll("\\)", "");
        regexp = regexp.replaceAll("ЗАО\\s", "");
        regexp = regexp.replaceAll("ОАО\\s", "");
        regexp = regexp.replaceAll("ГК\\s", "");
        regexp = regexp.replaceAll("ЖК\\s", "");
        regexp = regexp.replaceAll("ЖСК\\s", "");
        regexp = regexp.replaceAll("ИСК\\s", "");
        regexp = regexp.replaceAll("Группа Компаний\\s", "");
        regexp = regexp.replaceAll("«", "");
        regexp = regexp.replaceAll("»", "");
        regexp = regexp.replaceAll("\\s", "-");
        regexp = regexp.replaceAll(",", "");
        regexp = regexp.replaceAll("\\.$", "");
        regexp = regexp.replaceAll("(.*?)", "");

        return cyr2lat(regexp);
    }

    private static String cyr2lat(char ch){
        switch (ch){
            case 'а': return "a";
            case 'б': return "b";
            case 'в': return "v";
            case 'г': return "g";
            case 'д': return "d";
            case 'е': return "e";
            case 'ё': return "je";
            case 'ж': return "zh";
            case 'з': return "z";
            case 'и': return "i";
            case 'й': return "y";
            case 'к': return "k";
            case 'л': return "l";
            case 'м': return "m";
            case 'н': return "n";
            case 'о': return "o";
            case 'п': return "p";
            case 'р': return "r";
            case 'с': return "s";
            case 'т': return "t";
            case 'у': return "u";
            case 'ф': return "f";
            case 'х': return "kh";
            case 'ц': return "c";
            case 'ч': return "ch";
            case 'ш': return "sh";
            case 'щ': return "jsh";
            case 'ъ': return "hh";
            case 'ы': return "ih";
            case 'ь': return "jh";
            case 'э': return "eh";
            case 'ю': return "ju";
            case 'я': return "ja";
            default: return String.valueOf(ch);
        }
    }

    private static String cyr2lat(String s){
        StringBuilder sb = new StringBuilder(s.length()*2);
        for(char ch: s.toLowerCase().toCharArray()){
            sb.append(cyr2lat(ch));
        }
        return sb.toString();
    }
}
