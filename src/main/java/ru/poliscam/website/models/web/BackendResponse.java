package ru.poliscam.website.models.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import ru.poliscam.website.enums.BackendAPIResponseCode;

@Getter
@Setter
@NoArgsConstructor
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BackendResponse {

    private int code = BackendAPIResponseCode.OK.getCode();
    private Object data;
    private Long time;
    private String text;

    private BackendResponse(String text) {
        this.text = text;
    }

    private BackendResponse(Object data) {
        this.data = data;
    }

    private BackendResponse(BackendAPIResponseCode code, String text) {
        this.code = code.getCode();
        this.text = text;
    }

    private BackendResponse(BackendAPIResponseCode code, String text, Object data) {
        this.code = code.getCode();
        this.text = text;
        this.data = data;
    }

    public static BackendResponse of(String text) {
        return new BackendResponse(text);
    }

    public static BackendResponse of(Object data) {
        return new BackendResponse(data);
    }

    public static BackendResponse of(BackendAPIResponseCode code, String text) {
        return new BackendResponse(code, text);
    }

    public static BackendResponse of(BackendAPIResponseCode code, String text, Object data) {
        return new BackendResponse(code, text, data);
    }
}
