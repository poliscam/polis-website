package ru.poliscam.website.models.web.request;

import lombok.*;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class TokenRequest {
    private String token;
}
