package ru.poliscam.website.models.elastic;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Document(indexName="search", type="search", shards = 1, replicas = 0, refreshInterval = "-1")
@Setting(settingPath = "/elastic/settings.json")
@Mapping(mappingPath = "/elastic/mappings.json")
public class SearchedObject {
    @Id
    @Field(analyzer = "nGram_analyzer", searchAnalyzer = "whitespace_analyzer", store = true, type = FieldType.Text)
    private String name;

    @Field(type = FieldType.Long, index = false)
    private Long objectId;

    @Field(type = FieldType.Text, index = false)
    private String seoName;

    @Field(type = FieldType.Text, index = false)
    private String image;

    @Field(type = FieldType.Text, index = false)
    private String cityName;

    @Enumerated(EnumType.STRING)
    private Type type;

    @Field(type = FieldType.Integer, index = false)
    private Long views;

    public enum Type {
        COMPLEX,
        DEVELOPER
    }
}
