package ru.poliscam.website.models.messaging;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class HeartbeatMessage implements Serializable {
    private static final long serialVersionUID = 6399084355529116645L;
    private Long identifier;
    private BigDecimal version;
    private String ip;
}
