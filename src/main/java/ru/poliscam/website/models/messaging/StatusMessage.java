package ru.poliscam.website.models.messaging;

import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class StatusMessage implements Serializable {
    private static final long serialVersionUID = 1537661566976723272L;
    private Long worker;
    private Long cameraId;
    private Status status;

    public enum Status {
        IMAGE_FETCHED,
        IMAGE_NOT_FETCHED,
    }
}
