package ru.poliscam.website.models.messaging;

import lombok.*;
import ru.poliscam.website.models.dto.CameraForWorkerDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class TasksMessage implements Serializable {
    private static final long serialVersionUID = 6299184755528116645L;
    private List<CameraForWorkerDTO> cameras = new ArrayList<>();
}
