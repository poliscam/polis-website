package ru.poliscam.website.models.messaging;

import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class VideoUploaderMessage implements Serializable {
    private static final long serialVersionUID = 1536661566966723272L;
    private Long worker;
    private List<UploadedVideo> videos = new ArrayList<>();

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    public static class UploadedVideo implements Serializable {
        private static final long serialVersionUID = 1536661577966723272L;
        private Date date;
        private Long cameraId;
        private String youtubeId;
        private UploadType type;

        public enum UploadType {
            DAILY,
            WEEKLY,
            ALL_TIME
        }
    }
}
