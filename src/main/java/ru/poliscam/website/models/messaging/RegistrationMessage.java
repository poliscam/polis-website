package ru.poliscam.website.models.messaging;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class RegistrationMessage implements Serializable {
    private static final long serialVersionUID = 6355084755528116645L;
    private Long identifier;
    private String ip;
    private BigDecimal version;
}
