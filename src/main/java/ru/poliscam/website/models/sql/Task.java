package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "model2_tasks")
@NoArgsConstructor
@Getter
@Setter
public class Task extends Model {
    // Заголовок задачи
    private String title;

    // Интервал, с которой будет запускаться задача
    private String period;

    // Класс
    private String clazz;

    // Активна ли?
    private boolean enabled = true;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(joinColumns = {@JoinColumn(name = "task_id")}, inverseJoinColumns
            = {@JoinColumn(name = "camera_id")}, name = "camera_tasks")
    private Set<Camera> cameras = new HashSet<>();
}