package ru.poliscam.website.models.sql;

import lombok.*;
import ru.poliscam.website.enums.VideoType;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_videos")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class Video extends Model {
    @Column(columnDefinition = "timestamp")
    private Date added;

    @ManyToOne(fetch = FetchType.LAZY)
    private Camera camera;

    @Column(name = "youtube_id")
    private String youtubeID;

    @Enumerated(EnumType.STRING)
    @Column(name = "video_type")
    private VideoType type = VideoType.DAILY;
}
