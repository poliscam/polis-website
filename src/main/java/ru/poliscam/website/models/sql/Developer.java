package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_developer")
@NoArgsConstructor
@Getter
@Setter
public class Developer extends Model {
    private String name;
    private String logo;

    @Lob
    private String description;

    private String address;
    private String phone;
    private String email;
    private String site;

    @Column(name = "foundAt")
    private String foundAt;

    @Column(name = "seoName")
    private String seoName;

    private Boolean enabled = true;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "developer")
    private Set<Complex> complexes = new HashSet<>();
}
