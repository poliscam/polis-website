package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nix on 06.02.2016.
 */

@Entity
@Table(name = "model2_locations")
@NoArgsConstructor
@Getter
@Setter
public class Location extends Model {
    private String name;

    @Lob
    private String description;

    @Column(name = "seoName")
    private String seoName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "location")
    private Set<Complex> complexes = new HashSet<>();
}
