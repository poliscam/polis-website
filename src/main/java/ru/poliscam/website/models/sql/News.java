package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.sql.Blob;
import java.util.Date;

/**
 * Created by nikolay.viguro on 26.02.14.
 */

@Entity
@Table(name = "model2_news")
@NoArgsConstructor
@Getter
@Setter
public class News extends Model {
    @Column(name = "postedAt")
    private Date postedAt = new Date();
    private String image;
    private String subject;

    @Lob
    private String text;
    private boolean pinned = false;
}
