package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.poliscam.website.enums.Type;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_cameras")
@NoArgsConstructor
@Getter
@Setter
public class Camera extends Model {
    @Column(columnDefinition = "timestamp")
    private Date added = new Date();

    @Column(columnDefinition = "timestamp")
    private Timestamp logdate;

    @ManyToOne(fetch = FetchType.LAZY)
    private Complex complex;

    @ManyToOne(fetch = FetchType.LAZY)
    private Server server;

    private String name = "Камера ";

    @Column(name = "internal_name")
    private String internalName;

    private Integer width = 800;
    private Integer height = 600;

    private Boolean enabled = true;

    @Column(name = "useCurl")
    private Boolean useCurl = false;

    @Column(name = "realtime_url")
    private String realtimeUrl;

    @Enumerated(EnumType.STRING)
    @Column(name = "view_type")
    private Type viewType;

    @Column(name = "view_url")
    private String viewUrl;

    @Column(name = "last_daily_video")
    private String lastDailyVideo;

    @Column(name = "last_fast_video")
    private String lastFastVideo;

    @Column(name = "last_week_video")
    private String lastWeekVideo;

    private Boolean archive = false;

    @Lob
    private String ffmpegScript;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(joinColumns = {@JoinColumn(name = "camera_id")},
            inverseJoinColumns = {@JoinColumn(name = "task_id")}, name = "model2_camera_tasks")
    private Set<Task> tasks = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "camera")
    private Set<Video> videos = new HashSet<>();
}
