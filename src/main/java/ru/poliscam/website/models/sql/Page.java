package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "model2_pages")
@NoArgsConstructor
@Getter
@Setter
public class Page extends Model {
    @Column(name = "postedAt")
    private Date postedAt = new Date();

    // Путь до страницы
    // Например в /pages/Test
    // pageName == Test
    @Column(name = "pageName")
    private String pageName;

    private Blob thumb;

    private String keywords;

    private String subject;

    @Lob
    private String description;

    @Lob
    private String text;

    private String source;

    private Integer views;
}
