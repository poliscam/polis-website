package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import ru.poliscam.website.enums.AccountType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * @author nix (24.02.2017)
 */

@Entity
@Table(name = "accounts")
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
@Getter
@Setter
public class Account extends Model implements Serializable {
    private static final long serialVersionUID = 42L;

    // EMail аккаунта
    @Column(name = "email", nullable = false)
    protected String email;

    @Column(name = "password", nullable = false)
    protected String password;

    @Column(name = "display_name", length = 64)
    private String displayName;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled = true;

    // Роли в системе (в бинарном виде)
    @Column(name = "binary_roles", nullable = false)
    private Long binaryRoles = 0L;

    // Дата регистрации
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date regDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordReset;

    private String photoUrl;
    private String birthday;

    @OneToMany(mappedBy = "account", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FavoriteComplex> favoriteComplexes = new ArrayList<>();

    public void setAuthority(Set<AccountType> roles) {
        binaryRoles = 0L;
        for (AccountType role : roles)
            binaryRoles |= 1 << role.ordinal();
    }
}
