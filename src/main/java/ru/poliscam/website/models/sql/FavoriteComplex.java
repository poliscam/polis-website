package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_favorite_complexes")
@NoArgsConstructor
@Getter
@Setter
public class FavoriteComplex extends Model {
    @Column(name = "complexId")
    private Long complexId;

    @ManyToOne
    private Account account;
}
