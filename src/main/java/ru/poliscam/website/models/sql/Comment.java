package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_comments")
@NoArgsConstructor
@Getter
@Setter
public class Comment extends Model {
    @Column(columnDefinition = "timestamp")
    private Date added = new Date();

    @ManyToOne(fetch = FetchType.LAZY)
    private Complex complex;

    private Long specialId;

    @ManyToOne(fetch = FetchType.LAZY)
    private Account account;

    @Lob
    private String text;

    @Lob
    private String reply;

    @Column(columnDefinition = "timestamp")
    private Date replyAdded;

    private String ip;
}
