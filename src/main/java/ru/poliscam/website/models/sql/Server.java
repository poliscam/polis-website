package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;


@Entity
@Table(name = "model2_servers")
@NoArgsConstructor
@Getter
@Setter
public class Server extends Model {
    private String name;
    private String ip;
    private BigDecimal version;

    private Boolean enabled = false;

    @Lob
    private String additionalConfig;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "server")
    private Set<Camera> cameras = new HashSet<>();
}
