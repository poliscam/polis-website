package ru.poliscam.website.models.sql;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@Entity
@Table(name = "model2_complex")
@NoArgsConstructor
@Getter
@Setter
public class Complex extends Model {
    private String name;

    @Column(name = "metroWalkTime")
    private String metroWalkTime;

    @Column(name = "metro")
    private String metro;

    private String address;

    @Column(name = "ready_at")
    private String readyAt;

    @Column(name = "buildingClass")
    private String buildingClass;

    @Column(name = "buildingType")
    private String buildingType;

    private String floors;
    private String flats;
    private String cover;

    @Column(name = "priceRange")
    private String priceRange;

    @Column(name = "contractType")
    private String contractType;
    private String parking;

    @Lob
    private String description;

    @Lob
    private String announcement;

    @ManyToOne
    private Location location;

    private String logo;

    private String keywords;

    private Integer votes = 0;

    @ManyToOne
    private Developer developer;

    private Boolean enabled = true;

    @ManyToOne
    private City city;

    private String social;

    private Boolean archive = false;

    @Column(name = "archiveReason")
    private String archiveReason = "Строительство комплекса окончено";

    private Long views = 0L;

    @Column(name = "seoName")
    private String seoName;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "complex")
    @OrderBy("name ASC")
    private Set<Camera> cameras = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "complex")
    @OrderBy("added DESC")
    private Set<Comment> comments = new HashSet<>();
}
