package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WorkerInformationDTO implements DTO, Serializable, Comparable<WorkerInformationDTO> {
    private static final long serialVersionUID = 3788768199433195174L;

    private Long id;
    private String name;
    private String ip;
    private BigDecimal version;
    private Boolean enabled;
    private Long camerasCount;

    private String additionalConfig;

    // internal from db below

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime lastSeen;

    @Override
    public int compareTo(WorkerInformationDTO o) {
        return this.id.compareTo(o.id);
    }

    public boolean isAlive() {
        if (getLastSeen() == null || Duration.between(getLastSeen(), LocalDateTime.now()).getSeconds() > 30 * 60) {
            return false;
        } else {
            return true;
        }
    }

    public void merge(WorkerInformationDTO from) {
        if(!StringUtils.isEmpty(from.getName())) {
            this.name = from.getName();
        }

        if(!StringUtils.isEmpty(from.getAdditionalConfig())) {
            this.additionalConfig = from.getAdditionalConfig();
        }

        if(!StringUtils.isEmpty(from.getId())) {
            this.ip = from.getIp();
        }

        if(lastSeen != null) {
            this.lastSeen = from.getLastSeen();
        }

        if(version != null) {
            this.version = from.getVersion();
        }


    }
}
