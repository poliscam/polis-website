package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FavoriteComplexDTO implements DTO, Serializable {
    private static final long serialVersionUID = 2567302536797703873L;
    private Long complexId;
}
