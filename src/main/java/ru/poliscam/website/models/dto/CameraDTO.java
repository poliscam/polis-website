package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import ru.poliscam.website.enums.Type;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode
public class CameraDTO implements DTO, Serializable, Comparable<CameraDTO> {
    private static final long serialVersionUID = 3788768188433195174L;
    private Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date added;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date logdate;

    private Long complexId;
    private String complexName;

    private String name;
    private String internalName;

    private Integer width = 800;
    private Integer height = 600;
    private Boolean enabled = true;

    private Boolean useCurl = false;

    private String realtimeUrl;
    private Type viewType;

    private String lastDailyVideo;
    private String lastFastVideo;
    private String lastWeekVideo;

    private Boolean archive = false;

    private String ffmpegScript;

    private ServerForCameraDTO server;

    private Set<TaskDTO> tasks = new HashSet<>();

    // DTO specific fields
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastSuccessImageFetch;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastFailedImageFetch;

    @Override
    public int compareTo(CameraDTO o) {
        return this.name.compareTo(o.name);
    }

    @AllArgsConstructor
    @NoArgsConstructor
    @Getter
    @Setter
    @EqualsAndHashCode
    public static class ServerForCameraDTO implements Serializable {
        private static final long serialVersionUID = 4440261186917861369L;
        private Long id;
        private String name;
    }

    public void merge(CameraDTO from) {
        if(!StringUtils.isEmpty(from.getName())) {
            this.name = from.getName();
        }

        if(!StringUtils.isEmpty(from.getInternalName())) {
            this.internalName = from.getInternalName();
        }

        if(!StringUtils.isEmpty(from.getRealtimeUrl())) {
            this.realtimeUrl= from.getRealtimeUrl();
        }

        if(!StringUtils.isEmpty(from.getFfmpegScript())) {
            this.ffmpegScript = from.getFfmpegScript();
        }

        if(from.getArchive() != getArchive()) {
            this.archive = from.getArchive();
        }

        if(from.getEnabled() != getEnabled()) {
            this.enabled = from.getEnabled();
        }

        if(from.getUseCurl() != getUseCurl()) {
            this.useCurl = from.getUseCurl();
        }

        if(!from.getViewType().equals(getViewType())) {
            this.viewType = from.getViewType();
        }

        if((from.getServer() != null && !from.getServer().getId().equals(getServer().getId())) || (from.getServer() != null && getServer() == null)) {
            this.server = from.getServer();
        }

        if(!CollectionUtils.isEmpty(from.getTasks())) {
            this.tasks = from.getTasks();
        }

        if(from.getLastSuccessImageFetch() != null) {
            this.lastSuccessImageFetch = from.getLastSuccessImageFetch();
        }

        if(from.getLastFailedImageFetch() != null) {
            this.lastFailedImageFetch = from.getLastFailedImageFetch();
        }

        // todo

    }
}
