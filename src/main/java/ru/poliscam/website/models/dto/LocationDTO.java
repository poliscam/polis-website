package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LocationDTO implements Serializable, DTO {
    private static final long serialVersionUID = 5647782664607238396L;
    private Long id;
    private String name;
    private String description;
    private String seoName;
}
