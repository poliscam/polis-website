package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentDTO implements DTO, Serializable, Comparable<CommentDTO> {
    private static final long serialVersionUID = 2757311413911495781L;
    private Long id;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date added = new Date();

    private Long complexId;
    private AccountShortDTO account;
    private String text;
    private String reply;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private Date replyAdded;

    private String ip;

    @NoArgsConstructor
    @Getter
    @Setter
    @AllArgsConstructor
    @Builder
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class AccountShortDTO implements DTO, Serializable {
        private static final long serialVersionUID = 2757311413911466681L;
        private String email;
        private String name;
    }

    @Override
    public int compareTo(CommentDTO commentDTO) {
        return commentDTO.added.compareTo(this.added);
    }
}
