package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ComplexDTO implements DTO, Serializable {
    private static final long serialVersionUID = 2757361413911495781L;
    private Long id;
    private String name;
    private String metroWalkTime;
    private String metro;
    private String address;
    private String readyAt;
    private String buildingClass;
    private String buildingType;
    private String floors;
    private String flats;
    private String cover;
    private String priceRange;
    private String contractType;
    private String parking;
    private String description;
    private String announcement;
    private LocationDTO location;
    private String logo;
    private String keywords;
    private Integer votes = 0;
    private DeveloperDTO developer;
    private Boolean enabled;
    private CityDTO city;
    private String social;
    private Boolean archive;
    private String archiveReason;
    private Long views = 0L;
    private String seoName;

    private Set<CameraShortDTO> cameras = new TreeSet<>();

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class CameraShortDTO implements DTO, Serializable, Comparable<CameraShortDTO> {
        private static final long serialVersionUID = -1686970014447333666L;
        private Long id;
        private String name;
        private String lastFastVideo;
        private String lastWeekVideo;
        private String lastDailyVideo;
        private Boolean enabled;
        private Boolean archive;

        private String serverId;
        private String serverName;

        @Override
        public int compareTo(CameraShortDTO cameraShortDTO) {
            int i = this.getName().compareToIgnoreCase(cameraShortDTO.getName());

            if (this.getArchive() && !cameraShortDTO.getArchive()) {
                i += 100;
            } else if (!this.getArchive() && cameraShortDTO.getArchive()) {
                i -= 100;
            }

            return i;
        }
    }
}
