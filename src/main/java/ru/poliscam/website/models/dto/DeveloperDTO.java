package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by nikolay.viguro on 26.11.2015.
 */

@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeveloperDTO implements DTO, Serializable {
    private static final long serialVersionUID = -8579426319353756473L;
    private Long id;
    private String name;
    private String logo;
    private String description;
    private String address;
    private String phone;
    private String email;
    private String site;
    private String foundAt;
    private String seoName;
    private Boolean enabled;

    private Set<ComplexShortDTO> complexes = new TreeSet<>();

    @NoArgsConstructor
    @AllArgsConstructor
    @Builder
    @Getter
    @Setter
    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ComplexShortDTO implements DTO, Serializable, Comparable<ComplexShortDTO> {
        private static final long serialVersionUID = -1686970014447333266L;
        private Long id;
        private String name;
        private Boolean enabled;
        private Boolean archive;

        @Override
        public int compareTo(ComplexShortDTO cameraShortDTO) {
            return this.getName().compareToIgnoreCase(cameraShortDTO.getName());
        }
    }
}
