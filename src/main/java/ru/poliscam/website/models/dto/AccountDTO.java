package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.poliscam.website.enums.AccountType;
import ru.poliscam.website.models.sql.FavoriteComplex;
import ru.poliscam.website.models.sql.Model;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.util.*;

/**
 * @author nix (24.02.2017)
 */

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString
public class AccountDTO extends Model implements Serializable, DTO, UserDetails {
    private static final long serialVersionUID = 42L;

    @Email(message = "*Please provide a valid Email")
    @NotEmpty(message = "*Please provide an email")
    protected String email;

    @Length(min = 5, message = "*Your password must have at least 5 characters")
    @NotEmpty(message = "*Please provide your password")
    protected String password;

    @NotEmpty(message = "*Please provide your name")
    private String displayName;

    private Boolean enabled = true;

    private Long binaryRoles = 0L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date regDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordReset;

    private String photoUrl;

    private List<FavoriteComplex> favoriteComplexes = new ArrayList<>();

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<AccountType> roles = EnumSet.noneOf(AccountType.class);
        for (AccountType role : AccountType.values())
            if ((binaryRoles & (1 << role.ordinal())) != 0)
                roles.add(role);
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}
