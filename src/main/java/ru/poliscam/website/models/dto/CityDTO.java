package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CityDTO implements DTO, Serializable {
    private static final long serialVersionUID = -2616362771066466241L;
    private Long id;
    private String name;
}
