package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
public class VideoDTO implements Serializable, DTO {
    private static final long serialVersionUID = -1312924858552116374L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date added;

    private String youtubeID;
}
