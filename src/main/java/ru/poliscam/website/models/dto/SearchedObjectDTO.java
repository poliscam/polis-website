package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchedObjectDTO implements Serializable, DTO, Comparable<SearchedObjectDTO> {
    private static final long serialVersionUID = -3637113762923404339L;

    private Long id;
    private String name;
    private String seoName;
    private String image;
    private String cityName;
    private Type type;

    @JsonIgnore
    private Long views;

    @Override
    public int compareTo(SearchedObjectDTO that) {
        if (this.type.equals(Type.COMPLEX) && that.type.equals(Type.DEVELOPER)) {
            return -1;
        }
        if (this.type.equals(Type.DEVELOPER) && that.type.equals(Type.COMPLEX)) {
            return 1;
        }

        if (this.views > that.views) {
            return -1;
        }
        if (this.views < that.views) {
            return 1;
        }

        if (this.name.length() > that.views) {
            return -1;
        }
        if (this.name.length() < that.views) {
            return 1;
        }

        return 0;
    }

    public enum Type {
        COMPLEX,
        DEVELOPER
    }
}
