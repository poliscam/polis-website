package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Lob;
import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NewsDTO implements DTO, Serializable {
    private static final long serialVersionUID = -2646463771166466241L;
    public Long id;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date postedAt;

    private String image;
    private String subject;
    private String text;
    private Boolean pinned;
}
