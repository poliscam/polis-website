package ru.poliscam.website.models.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriptionDTO implements DTO, Serializable {
    private static final long serialVersionUID = -2616463771066466241L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date logdate;

    private String subscriptionId;
}
