package ru.poliscam.website.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 08.10.18
 */

@AllArgsConstructor
@Getter
public class Page {
    private int page = 1;
    private int size = 25;
}
