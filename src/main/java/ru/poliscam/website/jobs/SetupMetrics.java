package ru.poliscam.website.jobs;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.graphite.GraphiteMeterRegistry;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.utils.DiskSpaceMetrics;

import java.io.File;

@Component
@Log4j2
public class SetupMetrics {
    @Autowired(required = false)
    private GraphiteMeterRegistry registry;

    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    private final static File ROOT = new File("/");

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        if(registry == null) {
            return;
        }

        log.info("Setting up metrics");

        new DiskSpaceMetrics(ROOT).bindTo(registry);
        Gauge.builder("cache.workers", workers, c -> (double) workers.getAll().size()).register(registry);


        log.info("Done with setting up metrics");
    }
}
