package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.repositories.CameraDAO;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Component
@Log4j2
public class PopulateCamerasCacheJob {
    @Autowired
    private CameraDAO cameraDAO;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BackendCache<Long, CameraDTO> camerasCache;

    private Type listCamerasType = new TypeToken<List<CameraDTO>>() {
    }.getType();

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.refresh.globalcache}")
    public void run() {
        log.info("Loading cameras...");
        List<Camera> camerasList = cameraDAO.findByEnabledTrue();

        log.info("Converting cameras...");
        List<CameraDTO> camerasDTOList = mapper.map(camerasList, listCamerasType);

        log.info("Saving cameras to cache...");
        camerasCache.evictAll();
        camerasDTOList.forEach(cameraDTO -> camerasCache.put(cameraDTO.getId(), cameraDTO));
    }


}
