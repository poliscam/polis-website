package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.models.elastic.SearchedObject;
import ru.poliscam.website.models.sql.Complex;
import ru.poliscam.website.models.sql.Developer;
import ru.poliscam.website.repositories.ComplexDAO;
import ru.poliscam.website.repositories.DeveloperDAO;
import ru.poliscam.website.repositories.elastic.SearchDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Component
@Log4j2
public class PopulateSearchCacheJob {
    @Autowired
    private ComplexDAO complexDAO;

    @Autowired
    private DeveloperDAO developerDAO;

    @Autowired
    private SearchDAO searchDAO;

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.refresh.globalcache}")
    public void run() {
        log.info("Cleanup information in elasticsearch");

        elasticsearchTemplate.deleteIndex(SearchedObject.class);
        elasticsearchTemplate.createIndex(SearchedObject.class);
        elasticsearchTemplate.putMapping(SearchedObject.class);
        elasticsearchTemplate.refresh(SearchedObject.class);

        log.info("Loading search information to elasticsearch");

        List<SearchedObject> toSave = new ArrayList<>();
        List<Complex> complexList = complexDAO.findByEnabledTrue();
        List<Developer> developerList = developerDAO.findByEnabledTrue();

        complexList.forEach(complex -> {
            toSave.add(
              SearchedObject.builder()
                      .objectId(complex.getId())
                      .name(complex.getName())
                      .cityName(complex.getCity() != null ? complex.getCity().getName() : null)
                      .image(complex.getLogo())
                      .views(complex.getViews())
                      .seoName(complex.getSeoName())
                      .type(SearchedObject.Type.COMPLEX)
                      .build()
            );
        });

        developerList.forEach(developer -> {
            toSave.add(
                    SearchedObject.builder()
                            .objectId(developer.getId())
                            .name(developer.getName())
                            .seoName(developer.getSeoName())
                            .image(developer.getLogo())
                            .views(0L)
                            .type(SearchedObject.Type.DEVELOPER)
                            .build()
            );
        });

        searchDAO.saveAll(toSave);

        log.info("All entities loaded");
    }
}
