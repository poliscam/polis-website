package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;

import java.util.Set;

/**
 * @author Nikolay Viguro, 20.10.18
 */

@Component
@Log4j2
public class CheckCamerasAssignedToExistsWorkerJob {
    @Autowired
    private BackendCache<Long, WorkerInformationDTO> cacheWorkers;

    @Autowired
    private BackendCache<Long, CameraDTO> cacheCameras;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.camera.checkbelongs}")
    public void run() {
        log.info("Check cameras belongs to enabled workers...");

        Set<Long> workerIds = cacheWorkers.getAll().keySet();

        cacheCameras.getAll().forEach((id, camera) -> {
            if(camera.getServer() == null || !workerIds.contains(camera.getServer().getId())) {
                log.error("Camera with id #{} belongs to DEAD worker", id);
                // todo send message to slack
            }
        });
    }


}
