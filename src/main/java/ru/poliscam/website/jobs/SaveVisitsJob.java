package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.sql.Complex;
import ru.poliscam.website.repositories.ComplexDAO;

import javax.annotation.PreDestroy;
import java.util.Optional;

/**
 * @author Nikolay Viguro, 07.05.18
 */

@Component
@Log4j2
public class SaveVisitsJob {
    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ComplexDAO complexDAO;

    @Autowired
    private BackendCache<Long, Long> visitsCache;

    @Autowired
    private BackendCache<Long, ComplexDTO> complexCache;

    @PreDestroy
    public void shutdown() {
        run();
    }

    @Scheduled(cron = "${cron.save.visits}")
    public void run() {
        log.info("Saving visits...");

        visitsCache.getAll().forEach((complexId, visits) -> {
            Optional<Complex> complexOpt = complexDAO.findById(complexId);

            if (complexOpt.isPresent()) {
                Complex complex = complexOpt.get();
                complex.setViews(complex.getViews() + visits);
                complex = complexDAO.save(complex);

                ComplexDTO complexDTO = mapper.map(complex, ComplexDTO.class);

                complexCache.put(complexId, complexDTO);

                log.info("Complex #{}, visits: {}, total: {}", complexId, visits, complexDTO.getViews());
            } else {
                log.error("Complex #{} not exists!", complexId);
            }
        });

        visitsCache.evictAll();

        log.info("All visits saved");
    }


}
