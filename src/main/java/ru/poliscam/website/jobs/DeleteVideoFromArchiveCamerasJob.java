package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.sql.Camera;
import ru.poliscam.website.repositories.CameraDAO;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.List;

/**
 * @author Nikolay Viguro, 28.10.18
 */

@Component
@Log4j2
public class DeleteVideoFromArchiveCamerasJob {
    @Autowired
    private CameraDAO cameraDAO;

    @Value("${video.dir:/video}")
    private String videoDir;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.refresh.globalcache}")
    @Async
    public void run() {
        log.info("Cleanup archive...");

        cameraDAO.findByEnabledFalseOrArchiveTrue().forEach(camera -> {
            File regular = new File(videoDir + "/" + camera.getInternalName());
            File all = new File(videoDir + "/all/" + camera.getInternalName());

            try {
                if(Files.isDirectory(regular.toPath())) {
                    FileUtils.deleteDirectory(regular);
                    log.info("Directory deleted: {}", regular.toPath());
                }

                if(Files.isDirectory(all.toPath())) {
                    FileUtils.deleteDirectory(all);
                    log.info("Directory deleted: {}", regular.toPath());
                }
            } catch (IOException e) {
                log.error("Can't delete directory", e);
            }
        });

        log.info("Done with cleanup archive...");
    }
}
