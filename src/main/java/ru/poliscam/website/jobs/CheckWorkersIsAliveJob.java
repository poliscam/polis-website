package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.WorkerInformationDTO;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * @author Nikolay Viguro, 20.10.18
 */

@Component
@Log4j2
public class CheckWorkersIsAliveJob {
    @Value("${worker.heartbeat.ttl}")
    private Integer workerHeartbeatTTL = 30 * 60; // 30 minutes

    @Autowired
    private BackendCache<Long, WorkerInformationDTO> cache;

    @Scheduled(cron = "${cron.workers.checkalive}")
    public void run() {
        log.info("Check workers is alive...");

        cache.getAll().forEach((id, worker) -> {
            if (worker.getLastSeen() == null || Duration.between(worker.getLastSeen(), LocalDateTime.now()).getSeconds() > workerHeartbeatTTL) {
                log.error("Worker with ident #{} is dead (from {})!", id, worker.getLastSeen());
                // todo send message to slack
            }
        });
    }
}
