package ru.poliscam.website.jobs;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.repositories.ServerDAO;

/**
 * @author Nikolay Viguro, 20.10.18
 */

@Component
@Log4j2
public class PopulateWorkerCacheJob {
    @Autowired
    private ServerDAO serverDAO;

    @Autowired
    private ModelMapper mapper;

    @Autowired
    private BackendCache<Long, WorkerInformationDTO> cache;

    @EventListener(ApplicationReadyEvent.class)
    @Scheduled(cron = "${cron.refresh.globalcache}")
    public void run() {
        log.info("Loading workers information and convert...");
        serverDAO.findAll().forEach(server -> {
            WorkerInformationDTO worker = mapper.map(server, WorkerInformationDTO.class);

            if(cache.get(worker.getId()).isPresent()) {
                WorkerInformationDTO workerFromCache = cache.get(worker.getId()).get();
                worker.setLastSeen(workerFromCache.getLastSeen());
            }

            worker.setCamerasCount(server.getCameras().stream().filter(c -> c.getEnabled() && !c.getArchive()).count());

            cache.put(worker.getId(), worker);
        });

        log.info("Done with workers...");
    }


}
