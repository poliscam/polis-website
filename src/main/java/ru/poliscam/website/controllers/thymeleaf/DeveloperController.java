package ru.poliscam.website.controllers.thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.services.DeveloperService;

import java.util.Optional;

/**
 * @author nix (08.05.2018)
 */

@Controller
public class DeveloperController {
    @Autowired
    private DeveloperService developerService;

    @RequestMapping(value = "/d/{seoDeveloper}/info/{id}", method = RequestMethod.GET)
    public String showDeveloperInfo(@PathVariable("id") Long id, Model model) throws IllegalAccessException {
        Optional<DeveloperDTO> developerOpt = developerService.getDeveloper(id);

        if(!developerOpt.isPresent()) {
            throw new IllegalAccessException("Developer #" + id + " not found");
        } else {
            model.addAttribute("developer", developerOpt.get());
        }

        return "developerInfo";
    }
}
