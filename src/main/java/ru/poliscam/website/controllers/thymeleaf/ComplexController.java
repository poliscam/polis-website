package ru.poliscam.website.controllers.thymeleaf;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.services.CommentService;
import ru.poliscam.website.services.ComplexService;
import ru.poliscam.website.services.DeveloperService;

import java.util.List;
import java.util.Optional;

import static ru.poliscam.website.utils.HTMLTools.fillCommonData;

/**
 * @author nix (08.05.2018)
 */

@Controller
@Log4j2
public class ComplexController {
    @Autowired
    private DeveloperService developerService;

    @Autowired
    private ComplexService complexService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/d/{seo}/{id}", method = RequestMethod.GET)
    public String showComplexesByDeveloper(@PathVariable("id") Long id, Model model) throws IllegalAccessException {
        List<ComplexDTO> complexes = complexService.getComplexesByDeveloper(id);
        Optional<DeveloperDTO> developerOpt = developerService.getDeveloper(id);

        if(!developerOpt.isPresent()) {
            throw new IllegalAccessException("Developer #" + id + " not found");
        } else {
            model.addAttribute("developer", developerOpt.get());
        }

        model.addAttribute("complexes", complexes);

        return "complexesByDeveloper";
    }

    @RequestMapping(value = "/d/{seoDeveloper}/b/{seoComplex}/{complexId}", method = RequestMethod.GET)
    public String showComplexInfo(@PathVariable("complexId") Long id, Model model, Authentication auth) {
        Optional<ComplexDTO> complexOpt = complexService.getComplex(id);
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", id);
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(id);

        fillCommonData(complex, model, auth);

        model.addAttribute("developer", developer);
        model.addAttribute("complex", complex);
        model.addAttribute("comments", comments);

        return "complexInfo";
    }
}
