package ru.poliscam.website.controllers.thymeleaf.admin;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.enums.Type;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.dto.TaskDTO;
import ru.poliscam.website.repositories.TaskDAO;
import ru.poliscam.website.services.CameraService;
import ru.poliscam.website.services.ComplexService;
import ru.poliscam.website.services.WorkerService;

import java.util.Date;
import java.util.stream.Collectors;

/**
 * @author nix (30.10.2018)
 */

@Controller
@Log4j2
public class CameraAdminController {
    @Autowired
    private CameraService cameraService;

    @Autowired
    private ComplexService complexService;

    @Autowired
    private WorkerService workerService;

    @Autowired
    private TaskDAO taskDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private BackendCache<Long, ComplexDTO> complexCache;

    @RequestMapping(value = {"/admin/cameras/complex/{id}"}, method = RequestMethod.GET)
    public String camerasByComplex(Model model, @PathVariable("id") Long id) {
        model.addAttribute("complex", complexService.getComplex(id).orElse(null));
        model.addAttribute("cameras", cameraService.getCamerasByComplex(id));
        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{id}/camera/{cid}"}, method = RequestMethod.GET)
    public String camerasEdit(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        model.addAttribute("complex", complexService.getComplex(id).orElse(null));
        model.addAttribute("camera", cameraService.getCamera(cid));
        model.addAttribute("servers", workerService.getWorkers());
        model.addAttribute("tasks", taskDAO.findAll()
                .stream()
                .map(t -> modelMapper.map(t, TaskDTO.class))
                .collect(Collectors.toList()));
        return "admin/cameraEdit";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{cid}/delete/{id}"}, method = RequestMethod.GET)
    public String cameraDelete(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        cameraService.deleteCamera(id);
        complexCache.evict(cid);

        model.addAttribute("complex", complexService.getComplex(cid).orElse(null));
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{cid}/daily/{id}"}, method = RequestMethod.GET)
    public String cameraTaskDaily(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        ComplexDTO complexDTO = complexService.getComplex(cid).orElse(null);
        CameraDTO cameraDTO = cameraService.getCamera(id);

        model.addAttribute("complex", complexDTO);
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        log.info("Send create daily video command to worker #{} for camera #{}", cameraDTO.getServer().getId(), cameraDTO.getId());
        workerService.sendCreateDailyVideo(cameraDTO.getServer().getId(), cameraDTO);

        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{cid}/weekly/{id}"}, method = RequestMethod.GET)
    public String cameraTaskWeekly(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        ComplexDTO complexDTO = complexService.getComplex(cid).orElse(null);
        CameraDTO cameraDTO = cameraService.getCamera(id);

        model.addAttribute("complex", complexDTO);
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        log.info("Send create weekly video command to worker #{} for camera #{}", cameraDTO.getServer().getId(), cameraDTO.getId());
        workerService.sendCreateWeeklyVideo(cameraDTO.getServer().getId(), cameraDTO);

        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{cid}/alltime/{id}"}, method = RequestMethod.GET)
    public String cameraTaskAlltime(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        ComplexDTO complexDTO = complexService.getComplex(cid).orElse(null);
        CameraDTO cameraDTO = cameraService.getCamera(id);

        model.addAttribute("complex", complexDTO);
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        log.info("Send create alltime video command to worker #{} for camera #{}", cameraDTO.getServer().getId(), cameraDTO.getId());
        workerService.sendCreateAlltimeVideo(cameraDTO.getServer().getId(), cameraDTO);

        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{cid}/upload/{id}"}, method = RequestMethod.GET)
    public String cameraTaskUpload(Model model, @PathVariable("id") Long id, @PathVariable("cid") Long cid) {
        ComplexDTO complexDTO = complexService.getComplex(cid).orElse(null);
        CameraDTO cameraDTO = cameraService.getCamera(id);

        model.addAttribute("complex", complexDTO);
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        log.info("Send upload daily video command to worker #{} for camera #{}", cameraDTO.getServer().getId(), cameraDTO.getId());
        workerService.sendUploadDailyVideo(cameraDTO.getServer().getId(), cameraDTO);

        return "admin/cameras";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{id}/camera/{cid}"}, method = RequestMethod.POST)
    public String camerasEditSave(Model model, @ModelAttribute CameraDTO camera, @PathVariable("id") Long id,
                                  @PathVariable("cid") Long cid) {

        CameraDTO cameraDTOFromCache = cameraService.getCamera(cid);
        cameraDTOFromCache.merge(camera);

        if(cameraDTOFromCache.getViewType().equals(Type.IMAGE)) {
            cameraDTOFromCache.setUseCurl(true);
        } else {
            cameraDTOFromCache.setUseCurl(false);
        }

        cameraService.saveCamera(cameraDTOFromCache);

        if(cameraDTOFromCache.getServer() != null) {
            workerService.sendReload(cameraDTOFromCache.getServer().getId());
        }

        model.addAttribute("complex", complexService.getComplex(id).orElse(null));
        model.addAttribute("camera", cameraDTOFromCache);

        return "admin/cameraEdit";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{id}/new"}, method = RequestMethod.GET)
    public String camerasNew(Model model, @PathVariable("id") Long id) {
        model.addAttribute("complex", complexService.getComplex(id).orElse(null));
        model.addAttribute("camera", new CameraDTO());
        model.addAttribute("servers", workerService.getWorkers());
        model.addAttribute("tasks", taskDAO.findAll()
                .stream()
                .map(t -> modelMapper.map(t, TaskDTO.class))
                .collect(Collectors.toList()));
        return "admin/cameraNew";
    }

    @RequestMapping(value = {"/admin/cameras/complex/{сid}/new"}, method = RequestMethod.POST)
    public String camerasNewSave(Model model, @ModelAttribute CameraDTO camera, @PathVariable("сid") Long cid) {
        if(StringUtils.isEmpty(camera.getName()) || StringUtils.isEmpty(camera.getInternalName()) || StringUtils.isEmpty(camera.getRealtimeUrl())
        || camera.getServer() == null || camera.getServer().getId() == null) {
            log.error("Can't save new camera - wrong/incomplete data!");
            return "admin/cameraNew";
        }

        if(camera.getViewType().equals(Type.IMAGE)) {
            camera.setUseCurl(true);
        } else {
            camera.setUseCurl(false);
        }

        camera.setComplexId(cid);
        camera.setAdded(new Date());

        cameraService.saveCamera(camera);
        workerService.sendReload(camera.getServer().getId()); // todo send couple tasks for adding instead of reload

        model.addAttribute("complex", complexService.getComplex(cid).orElse(null));
        model.addAttribute("cameras", cameraService.getCamerasByComplex(cid));

        return "admin/cameras";
    }
}
