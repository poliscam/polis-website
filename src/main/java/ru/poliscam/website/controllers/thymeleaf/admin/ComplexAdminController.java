package ru.poliscam.website.controllers.thymeleaf.admin;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.services.ComplexService;
import ru.poliscam.website.services.SiteParserService;

import java.util.Optional;

/**
 * @author nix (30.10.2018)
 */

@Controller
@Log4j2
public class ComplexAdminController {
    @Autowired
    private ComplexService complexService;

    @Autowired
    private SiteParserService siteParserService;

    @RequestMapping(value = {"/admin/complexes"}, method = RequestMethod.GET)
    public String complexes(Model model) {
        model.addAttribute("complexes", complexService.getAllComplexes());
        return "admin/complexes";
    }

    @RequestMapping(value = {"/admin/complexes/{id}"}, method = RequestMethod.GET)
    public String complexEdit(Model model, @PathVariable("id") Long id) {
        model.addAttribute("complex", complexService.getComplex(id).orElse(null));
        return "admin/complexEdit";
    }

    @RequestMapping(value = {"/admin/complexes/delete/{id}"}, method = RequestMethod.GET)
    public String complexDelete(Model model, @PathVariable("id") Long id) {
        complexService.delete(id);
        model.addAttribute("complexes", complexService.getAllComplexes());
        return "admin/complexes";
    }

    @RequestMapping(value = {"/admin/complexes/parser"}, method = RequestMethod.GET)
    public String complexParser() {
        return "admin/complexParser";
    }

    @RequestMapping(value = {"/admin/complexes/parser"}, method = RequestMethod.POST)
    public String complexParser(Model model, @RequestParam("url") String url) {
        Optional<ComplexDTO> complexDTO = siteParserService.getComplex(url);
        complexDTO.ifPresent(complex -> model.addAttribute("complex", complex));
        return "admin/complexParser";
    }
}
