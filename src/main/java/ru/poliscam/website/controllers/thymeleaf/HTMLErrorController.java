package ru.poliscam.website.controllers.thymeleaf;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * @author Nikolay Viguro, 22.11.18
 */

@Controller
@Log4j2
public class HTMLErrorController implements ErrorController {
    @RequestMapping("/error")
    public String handleError(HttpServletRequest request) {
        log.error("Expect some error. Path: {}", request.getAttribute(RequestDispatcher.FORWARD_REQUEST_URI));
        return "error";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}