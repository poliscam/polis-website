package ru.poliscam.website.controllers.thymeleaf;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.ComplexDTO;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.services.CameraService;
import ru.poliscam.website.services.CommentService;
import ru.poliscam.website.services.ComplexService;

import java.util.Optional;

import static ru.poliscam.website.utils.HTMLTools.fillCommonData;

/**
 * @author nix (08.05.2018)
 */

@Controller
@Log4j2
public class CameraController {
    @Autowired
    private ComplexService complexService;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private CommentService commentService;

    @RequestMapping(value = "/b/{seo}/{id}", method = RequestMethod.GET)
    public String showDefaultCameraByComplex(@PathVariable("id") Long complexId, Model model, Authentication auth) {
        Optional<ComplexDTO> complexOpt = complexService.getComplex(complexId);
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", complexId);
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complexId);

        fillCommonData(complex, model, auth);

        model.addAttribute("developer", developer);
        model.addAttribute("complex", complex);
        model.addAttribute("comments", comments);

        if (complex.getCameras().size() > 0) {
            CameraDTO camera = cameraService.getCamera(complex.getCameras().iterator().next().getId());
            model.addAttribute("camera", camera);

            if (camera.getArchive()) {
                return "camerasFast";
            } else {
                return "cameras";
            }
        } else {
            return "camerasNone";
        }
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/b/{seo}/cam/{id}", method = RequestMethod.GET)
    public String showSelectedCamera(@PathVariable("id") Long id, Model model, Authentication auth) {
        CameraDTO camera = cameraService.getCamera(id);
        Optional<ComplexDTO> complexOpt = complexService.getComplex(camera.getComplexId());
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", camera.getComplexId());
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complex.getId());

        fillCommonData(complex, model, auth);

        model.addAttribute("camera", camera);
        model.addAttribute("complex", complex);
        model.addAttribute("developer", developer);
        model.addAttribute("comments", comments);

        if (camera.getArchive()) {
            return "camerasFast";
        } else {
            return "cameras";
        }
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/b/{seo}/byday/{id}", method = RequestMethod.GET)
    public String showSelectedCameraByDay(@PathVariable("id") Long id, Model model, Authentication auth) {
        CameraDTO camera = cameraService.getCamera(id);
        Optional<ComplexDTO> complexOpt = complexService.getComplex(camera.getComplexId());
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", camera.getComplexId());
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complex.getId());

        fillCommonData(complex, model, auth);

        model.addAttribute("camera", camera);
        model.addAttribute("complex", complex);
        model.addAttribute("developer", developer);
        model.addAttribute("comments", comments);

        return "camerasByDay";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/b/{seo}/week/{id}", method = RequestMethod.GET)
    public String showSelectedCameraWeekly(@PathVariable("id") Long id, Model model, Authentication auth) {
        CameraDTO camera = cameraService.getCamera(id);
        Optional<ComplexDTO> complexOpt = complexService.getComplex(camera.getComplexId());
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", camera.getComplexId());
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complex.getId());

        fillCommonData(complex, model, auth);

        model.addAttribute("camera", camera);
        model.addAttribute("complex", complex);
        model.addAttribute("developer", developer);
        model.addAttribute("comments", comments);

        if (camera.getArchive()) {
            return "camerasFast";
        } else {
            return "camerasWeek";
        }
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/b/{seo}/fast/{id}", method = RequestMethod.GET)
    public String showSelectedCameraFast(@PathVariable("id") Long id, Model model, Authentication auth) {
        CameraDTO camera = cameraService.getCamera(id);
        Optional<ComplexDTO> complexOpt = complexService.getComplex(camera.getComplexId());
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", camera.getComplexId());
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complex.getId());

        fillCommonData(complex, model, auth);

        model.addAttribute("camera", camera);
        model.addAttribute("complex", complex);
        model.addAttribute("developer", developer);
        model.addAttribute("comments", comments);

        return "camerasFast";
    }

    @SuppressWarnings("Duplicates")
    @RequestMapping(value = "/b/{seo}/live/{id}", method = RequestMethod.GET)
    public String showSelectedCameraLive(@PathVariable("id") Long id, Model model, Authentication auth) {
        CameraDTO camera = cameraService.getCamera(id);
        Optional<ComplexDTO> complexOpt = complexService.getComplex(camera.getComplexId());
        ComplexDTO complex;

        if(complexOpt.isPresent()) {
            complex = complexOpt.get();
        } else {
            log.error("Can't found complex with id #{}", camera.getComplexId());
            return "redirect:/";
        }

        DeveloperDTO developer = complex.getDeveloper();
        Long comments = commentService.getCommentsCount(complex.getId());

        fillCommonData(complex, model, auth);

        model.addAttribute("camera", camera);
        model.addAttribute("complex", complex);
        model.addAttribute("developer", developer);
        model.addAttribute("comments", comments);

        if (camera.getArchive()) {
            return "camerasFast";
        } else {
            return "camerasLive";
        }
    }
}
