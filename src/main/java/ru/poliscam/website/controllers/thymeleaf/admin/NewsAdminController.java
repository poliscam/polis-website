package ru.poliscam.website.controllers.thymeleaf.admin;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.NewsDTO;
import ru.poliscam.website.services.CameraService;
import ru.poliscam.website.services.ComplexService;
import ru.poliscam.website.services.NewsService;
import ru.poliscam.website.services.WorkerService;

/**
 * @author nix (11.11.2018)
 */

@Controller
@Log4j2
public class NewsAdminController {
    @Autowired
    private NewsService newsService;

    @RequestMapping(value = {"/admin/news"}, method = RequestMethod.GET)
    public String news(Model model) {
        model.addAttribute("news", newsService.getNews(PageRequest.of(0, 100)));
        return "admin/news";
    }

    @RequestMapping(value = {"/admin/news/new"}, method = RequestMethod.GET)
    public String newsCreate(Model model) {
        model.addAttribute("news", new NewsDTO());
        return "admin/newsCreate";
    }

    @RequestMapping(value = {"/admin/news/new"}, method = RequestMethod.POST)
    public String newsCreateSave(Model model, @ModelAttribute NewsDTO news) {
        if(news == null || news.getId() != null || StringUtils.isEmpty(news.getText()) || StringUtils.isEmpty(news.getSubject())) {
            log.error("Can't add news - incomplete/wrong data");
            return "redirect:/admin/news";
        }
        newsService.addNews(news);

        return "redirect:/admin/news";
    }
}
