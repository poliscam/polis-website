package ru.poliscam.website.controllers.thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.models.dto.NewsDTO;
import ru.poliscam.website.services.NewsService;

import java.util.Optional;

import static ru.poliscam.website.utils.HTMLTools.fillAuth;

/**
 * @author nix (08.05.2018)
 */

@Controller
public class NewsController {
    @Autowired
    private NewsService newsService;

    @RequestMapping(value = {"/news"}, method = RequestMethod.GET)
    public String news(Model model, Authentication auth) {
        fillAuth(model, auth);
        model.addAttribute("news", newsService.getNews(PageRequest.of(0, 20, Sort.Direction.DESC, "postedAt")));
        return "news";
    }

    @RequestMapping(value = {"/news/show/{nid}"}, method = RequestMethod.GET)
    public String newsLatest(Model model, Authentication auth, @PathVariable("nid") Long nid) {
        fillAuth(model, auth);

        Optional<NewsDTO> news = newsService.getNews(nid);
        if(news.isPresent()) {
            model.addAttribute("news", news.get());
            return "newsShow";
        } else {
            return "error";
        }
    }

    @RequestMapping(value = {"/news/last"}, method = RequestMethod.GET)
    public String newsLatest(Model model, Authentication auth) {
        fillAuth(model, auth);

        Optional<NewsDTO> news = newsService.getLastNews();
        if(news.isPresent()) {
            model.addAttribute("news", news.get());
            return "newsShow";
        } else {
            return "error";
        }
    }
}
