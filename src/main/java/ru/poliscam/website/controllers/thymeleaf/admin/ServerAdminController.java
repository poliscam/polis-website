package ru.poliscam.website.controllers.thymeleaf.admin;

import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.poliscam.website.cache.BackendCache;
import ru.poliscam.website.models.dto.CameraDTO;
import ru.poliscam.website.models.dto.WorkerInformationDTO;
import ru.poliscam.website.models.sql.Server;
import ru.poliscam.website.repositories.ServerDAO;
import ru.poliscam.website.services.CameraService;
import ru.poliscam.website.services.WorkerService;

/**
 * @author nix (01.11.2018)
 */

@Controller
@Log4j2
public class ServerAdminController {
    @Autowired
    private BackendCache<Long, WorkerInformationDTO> workers;

    @Autowired
    private CameraService cameraService;

    @Autowired
    private ServerDAO serverDAO;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WorkerService workerService;

    @RequestMapping(value = {"/admin/servers"}, method = RequestMethod.GET)
    public String servers(Model model) {
        model.addAttribute("servers", workers.getAll().values());
        return "admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/cameras/{id}"}, method = RequestMethod.GET)
    public String serverCameras(Model model, @PathVariable("id") Long id) {
        model.addAttribute("cameras", cameraService.getCamerasByServer(id));
        return "admin/serverCameras";
    }

    @RequestMapping(value = {"/admin/servers/new"}, method = RequestMethod.GET)
    public String serverNew(Model model) {
        model.addAttribute("server", new WorkerInformationDTO());
        return "admin/serversNew";
    }

    @RequestMapping(value = {"/admin/servers/{id}"}, method = RequestMethod.GET)
    public String serverEdit(Model model, @PathVariable("id") Long id) {
        model.addAttribute("server", workers.get(id).orElse(new WorkerInformationDTO()));
        return "admin/serversEdit";
    }

    @RequestMapping(value = {"/admin/servers/{id}"}, method = RequestMethod.POST)
    public String serverEditSave(Model model, @PathVariable("id") Long id, @ModelAttribute WorkerInformationDTO server) {
        if(server == null || server.getId() == null || StringUtils.isEmpty(server.getName()) || StringUtils.isEmpty(server.getAdditionalConfig())) {
            log.error("Can't edit worker - incomplete data");
            return "redirect:/admin/servers";
        }

        WorkerInformationDTO inCache = workers.get(id).orElse(server);

        server.merge(inCache);

        serverDAO.save(modelMapper.map(server, Server.class));
        workers.put(id, server);
        workerService.sendReload(id);

        return "redirect:/admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/new"}, method = RequestMethod.POST)
    public String serverNewSave(Model model, @ModelAttribute WorkerInformationDTO server) {
        if(server == null || server.getId() != null || StringUtils.isEmpty(server.getName()) || StringUtils.isEmpty(server.getAdditionalConfig())) {
            log.error("Can't add worker - incomplete/wrong data");
            return "redirect:/admin/servers";
        }

        Server serverDb = serverDAO.save(modelMapper.map(server, Server.class));
        workers.put(server.getId(), modelMapper.map(serverDb, WorkerInformationDTO.class));

        return "redirect:/admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/restart/{id}"}, method = RequestMethod.GET)
    public String serverRestart(Model model, @PathVariable("id") Long id) {
        workerService.sendRestart(id);
        return "redirect:/admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/reload/{id}"}, method = RequestMethod.GET)
    public String serverReload(Model model, @PathVariable("id") Long id) {
        workerService.sendReload(id);
        return "redirect:/admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/update/{id}"}, method = RequestMethod.GET)
    public String serverUpdate(Model model, @PathVariable("id") Long id) {
        workerService.sendUpdate(id);
        return "redirect:/admin/servers";
    }

    @RequestMapping(value = {"/admin/servers/delete/{id}"}, method = RequestMethod.GET)
    public String serverDelete(Model model, @PathVariable("id") Long id) {

        serverDAO.deleteById(id);
        workers.evict(id);

        return "redirect:/admin/servers";
    }
}
