package ru.poliscam.website.controllers.thymeleaf.admin;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author nix (30.10.2018)
 */

@Controller
@Log4j2
public class DashboardAdminController {
    @RequestMapping(value = {"/admin", "/admin/", "/admin/dashboard"}, method = RequestMethod.GET)
    public String dashboard(Model model) {
        return "admin/dashboard";
    }
}
