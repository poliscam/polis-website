package ru.poliscam.website.controllers.thymeleaf;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.services.AccountService;
import ru.poliscam.website.services.CaptchaService;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * @author nix (11.10.2018)
 */

@Controller
@Log4j2
public class AccountController {
    @Autowired
    private AccountService accountService;

    @Autowired
    private CaptchaService captchaService;

    @RequestMapping(value = {"/login"}, method = RequestMethod.GET)
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration(ModelAndView modelAndView) {
        modelAndView.addObject("user", new AccountDTO());
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(ModelAndView modelAndView, @Valid AccountDTO user, BindingResult bindingResult,
                                      @RequestParam("g-recaptcha-response") String captcha,
                                      HttpServletRequest request) {
        String remoteAddr = null;
        modelAndView.setViewName("registration");

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }

        if (!captchaService.isCaptchaValid(captcha, remoteAddr)) {
            log.error("Recaptcha fail for {}", remoteAddr);
            modelAndView.addObject("errorMessage", "Ошибка распознавания капчи.");
            return modelAndView;
        }


        log.info("Creating user: {}", user);
        AccountDTO userExists = accountService.findAccount(user.getEmail());
        if (userExists != null) {
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            log.info("Reg form has errors");
            modelAndView.addObject("errorMessage", "Ошибка заполнения формы.");
        } else {
            log.info("Saving user");
            accountService.saveAccount(user);
            modelAndView.addObject("successMessage", "Вы успешно зарегистрированы. Теперь войдите на сайт.");
            modelAndView.addObject("user", new AccountDTO());
        }
        return modelAndView;
    }
}
