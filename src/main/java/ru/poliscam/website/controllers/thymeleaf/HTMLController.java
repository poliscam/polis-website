package ru.poliscam.website.controllers.thymeleaf;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.poliscam.website.models.dto.DeveloperDTO;
import ru.poliscam.website.services.CommentService;
import ru.poliscam.website.services.DeveloperService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static ru.poliscam.website.utils.HTMLTools.fillAuth;

/**
 * @author nix (08.05.2018)
 */

@Controller
public class HTMLController {
    @Autowired
    private DeveloperService developerService;

    @Autowired
    private CommentService commentService;

    @Value("${comment.id.contacts}")
    private Long commentId;

    @RequestMapping(value = {"/", "/index", "/a/{seoName}"}, method = RequestMethod.GET)
    public String index(Model model, Authentication auth) {
        List<DeveloperDTO> developers = developerService.getDevelopers(0, 20);
        fillAuth(model, auth);
        model.addAttribute("developers", developers);
        return "index";
    }

    @RequestMapping(value = {"/hear-you", "/contacts"}, method = RequestMethod.GET)
    public String contacts(Model model, Authentication auth) {
        Long comments = commentService.getCommentsCount(commentId);

        fillAuth(model, auth);
        model.addAttribute("comments", comments);
        model.addAttribute("commentId", commentId);

        return "hearYou";
    }

    @RequestMapping(value = {"/pages"}, method = RequestMethod.GET)
    public String pages(Model model, Authentication auth) {
        fillAuth(model, auth);
        return "pages";
    }

    @RequestMapping(value = {"/robots.txt"}, method = RequestMethod.GET)
    @ResponseBody
    public String robots(HttpServletResponse response) {
        response.setContentType("text/plain");
        response.setCharacterEncoding("UTF-8");
        return "User-agent: *\n" +
                "Disallow: /admin";
    }

    @RequestMapping(value = {"/firebase-messaging-sw.js"}, method = RequestMethod.GET, produces = "text/javascript")
    @ResponseBody
    public String sw(HttpServletResponse response) throws IOException {
        response.setContentType("text/javascript");
        response.setCharacterEncoding("UTF-8");
        return new String(Files.readAllBytes(Paths.get("html/static/js/firebase-messaging-sw.js")));
    }
}
