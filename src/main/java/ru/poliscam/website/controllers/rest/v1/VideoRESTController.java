package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.poliscam.website.enums.BackendAPIResponseCode;
import ru.poliscam.website.enums.VideoType;
import ru.poliscam.website.models.dto.VideoDTO;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.services.VideoService;

import java.util.Date;
import java.util.List;

@Log4j2
@RestController
@RequestMapping("/api/v1/video")
public class VideoRESTController {
    @Autowired
    private VideoService videoService;

    @GetMapping("/cam/{id}/date/{date}/type/{type}")
    public BackendResponse getCameraById(
            @PathVariable("id") Long id,
            @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
            @PathVariable("type") VideoType type) {
        List<VideoDTO> videosDTO = videoService.getVideos(id, date, type);

        if (CollectionUtils.isEmpty(videosDTO)) {
            return BackendResponse.builder()
                    .code(BackendAPIResponseCode.NOT_FOUND.getCode())
                    .text("Not found")
                    .build();
        } else {
            return BackendResponse.of(videosDTO.get(0));
        }
    }
}
