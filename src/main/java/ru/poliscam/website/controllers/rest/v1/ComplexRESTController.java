package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.services.ComplexService;

@RestController
@Log4j2
@RequestMapping("/api/v1/complex")
public class ComplexRESTController {
    @Autowired
    private ComplexService complexService;

    @GetMapping("/{id}")
    public BackendResponse getComplexById(@PathVariable("id") Long id) {
        return BackendResponse.of(complexService.getComplex(id));
    }
}
