package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.services.CameraService;

@Log4j2
@RestController
@RequestMapping("/api/v1/camera")
public class CameraRESTController {
    @Autowired
    private CameraService cameraService;

    @GetMapping("/{id}")
    public BackendResponse getCameraById(@PathVariable("id") Long id) {
        return BackendResponse.of(cameraService.getCamera(id));
    }

    @GetMapping("/complex/{id}")
    public BackendResponse getCameraByComplexId(@PathVariable("id") Long id) {
        return BackendResponse.of(cameraService.getCamerasByComplex(id));
    }
}
