package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.models.web.request.TokenRequest;
import ru.poliscam.website.services.SubscriptionService;

@Log4j2
@RestController
@CrossOrigin
@RequestMapping("/api/v1/subscription")
public class SubscriptionRESTController {
    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping("/add")
    public BackendResponse add(@RequestBody TokenRequest token) {
        subscriptionService.addSubscription(token.getToken());
        return BackendResponse.of("ok");
    }
}