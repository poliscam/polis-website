package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.poliscam.website.enums.BackendAPIResponseCode;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.services.SearchService;

import static ru.poliscam.website.enums.AccountType.ADMIN;

@Log4j2
@RestController
@RequestMapping("/api/v1/search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping
    public BackendResponse search(@RequestParam("q") String q) {
        return BackendResponse.of(searchService.getResults(q));
    }

    @GetMapping("/reload")
    public BackendResponse reloadCache(Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated() && authentication.getAuthorities().contains(ADMIN)) {
            log.info("Admin request the search cache reload");
            searchService.reload();
            return BackendResponse.of("ok");
        } else {
            log.info("Not auth when try to reload search cache");
            return BackendResponse.of(BackendAPIResponseCode.UNAUTHORIZED, "not authenticated");
        }
    }
}