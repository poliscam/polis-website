package ru.poliscam.website.controllers.rest.v1;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.poliscam.website.enums.BackendAPIResponseCode;
import ru.poliscam.website.models.Page;
import ru.poliscam.website.models.dto.AccountDTO;
import ru.poliscam.website.models.dto.CommentDTO;
import ru.poliscam.website.models.web.BackendResponse;
import ru.poliscam.website.services.CommentService;
import ru.poliscam.website.services.EmailService;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

import static ru.poliscam.website.enums.AccountType.ADMIN;

@Log4j2
@RestController
@RequestMapping("/api/v1/comment")
public class CommentRESTController {

    @Autowired
    private CommentService commentService;

    @Autowired
    private EmailService emailService;

    @GetMapping("/complex/{id}/page/{page}/size/{size}")
    public BackendResponse getComments(
            @PathVariable("id") Long id,
            @PathVariable("page") Integer page,
            @PathVariable("size") Integer size) {
        return BackendResponse.of(
                commentService.getCommentByComplex(id, new Page(page, size))
        );
    }

    @PostMapping("/complex/{id}")
    public BackendResponse addCommentToComplex(
            @PathVariable("id") Long id,
            @RequestParam("text") String text,
            Authentication authentication,
            HttpServletRequest request) {

        if (authentication != null && authentication.isAuthenticated()) {
            String remoteAddr = null;
            if (request != null) {
                remoteAddr = request.getHeader("X-FORWARDED-FOR");
                if (remoteAddr == null || "".equals(remoteAddr)) {
                    remoteAddr = request.getRemoteAddr();
                }
            }

            AccountDTO accountDTO = (AccountDTO) authentication.getPrincipal();

            CommentDTO commentDTO = CommentDTO.builder()
                    .complexId(id)
                    .account(new CommentDTO.AccountShortDTO(accountDTO.getEmail(), accountDTO.getDisplayName()))
                    .text(text)
                    .ip(remoteAddr)
                    .build();

            commentService.addComment(commentDTO);

            log.info("Added comment to complex #{}. Name: \"{}\", text: \"{}\"", id, authentication.getName(), text);

            return BackendResponse.of("ok");
        } else {
            log.info("Not auth when try to add comment to complex #{}. Text: \"{}\"", id, text);
            return BackendResponse.of(BackendAPIResponseCode.UNAUTHORIZED, "not authenticated");
        }
    }

    @PostMapping("/reply/{id}")
    public BackendResponse addReplyToComment(
            @PathVariable("id") Long id,
            @RequestParam("text") String text,
            Authentication authentication) {

        if (authentication != null && authentication.isAuthenticated() && authentication.getAuthorities().contains(ADMIN)) {
            Optional<CommentDTO> commentDTOOptional = commentService.getComment(id);

            if(commentDTOOptional.isPresent()) {
                CommentDTO commentDTO = commentDTOOptional.get();

                commentDTO.setReply(text);
                commentDTO.setReplyAdded(new Date());

                commentService.saveComment(commentDTO);
                emailService.sendText(
                        commentDTO.getAccount().getEmail(),
                        "На ваш комментарий на сайте PolisCam.Ru ответил администратор"
                );

                log.info("Replied to comment #{}. Text: \"{}\"", id, text);

                return BackendResponse.of("ok");
            } else {
                return BackendResponse.of(BackendAPIResponseCode.NOT_FOUND, "Comment with id " + id + " is not found");
            }
        } else {
            log.info("Not auth when try to add reply comment #{}. Text: \"{}\"", id, text);
            return BackendResponse.of(BackendAPIResponseCode.UNAUTHORIZED, "not authenticated");
        }
    }

    @GetMapping("/delete/complex/{cid}/comment/{id}")
    public BackendResponse deleteComment(
            @PathVariable("cid") Long cid,
            @PathVariable("id") Long id,
            Authentication authentication) {

        if (authentication != null && authentication.isAuthenticated() && authentication.getAuthorities().contains(ADMIN)) {
            commentService.deleteComment(cid, id);

            log.info("Deleted comment #{}.", id);

            return BackendResponse.of("ok");
        } else {
            log.info("Not auth when try to delete comment", id);
            return BackendResponse.of(BackendAPIResponseCode.UNAUTHORIZED, "not authenticated");
        }
    }
}
