package ru.poliscam.website.repositories.elastic;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;
import ru.poliscam.website.models.elastic.SearchedObject;

@Repository
public interface SearchDAO extends ElasticsearchRepository<SearchedObject, String> {
}
