package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.enums.VideoType;
import ru.poliscam.website.models.sql.Video;

import java.util.Date;
import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

public interface VideoDAO extends JpaRepository<Video, Long> {
    List<Video> findByAddedBetween(Date addedStart, Date addedStop);

    List<Video> findByCameraIdAndTypeAndAddedBetween(Long cameraId, VideoType type, Date addedStart, Date addedStop);
}
