package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Complex;

import java.util.List;
import java.util.Optional;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface ComplexDAO extends JpaRepository<Complex, Long> {
    List<Complex> findByEnabledTrue();

    List<Complex> findByEnabledTrueAndDeveloperId(Long id);

    Optional<Complex> findByName(String name);
}
