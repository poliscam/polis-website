package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Task;

/**
 * @author Nikolay Viguro, 15.11.18
 */

public interface TaskDAO extends JpaRepository<Task, Long> {
}
