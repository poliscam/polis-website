package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Camera;

import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

public interface CameraDAO extends JpaRepository<Camera, Long> {
    List<Camera> findByEnabledTrue();
    List<Camera> findByEnabledFalseOrArchiveTrue();
    List<Camera> findByServerId(Long id);
}
