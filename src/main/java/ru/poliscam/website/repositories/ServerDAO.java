package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Server;

import java.util.List;

/**
 * @author Nikolay Viguro, 20.10.18
 */

public interface ServerDAO extends JpaRepository<Server, Long> {
    List<Server> findAllByEnabledTrue();
}
