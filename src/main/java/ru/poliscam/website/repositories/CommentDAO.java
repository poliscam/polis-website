package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Comment;

import java.util.List;

/**
 * @author Nikolay Viguro, 07.05.18
 */

public interface CommentDAO extends JpaRepository<Comment, Long> {
    List<Comment> findByComplexIdOrderByAddedDesc(Long complexId);

    List<Comment> findBySpecialIdOrderByAddedDesc(Long specialId);

    Long countByComplexId(Long complexId);

    Long countBySpecialId(Long specialId);
}
