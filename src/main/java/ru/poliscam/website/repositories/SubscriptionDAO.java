package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Subscription;

import java.util.Optional;

/**
 * @author Nikolay Viguro, 10.11.18
 */

public interface SubscriptionDAO extends JpaRepository<Subscription, Long> {
    Optional<Subscription> findBySubscriptionId(String id);
}
