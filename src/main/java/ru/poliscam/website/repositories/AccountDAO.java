package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Account;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface AccountDAO extends JpaRepository<Account, Long> {
    Account findByEmail(String email);
}
