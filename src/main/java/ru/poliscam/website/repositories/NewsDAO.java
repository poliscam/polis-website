package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.News;

/**
 * @author Nikolay Viguro, 11.11.18
 */

public interface NewsDAO extends JpaRepository<News, Long> {
}
