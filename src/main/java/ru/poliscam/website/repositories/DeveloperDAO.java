package ru.poliscam.website.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Developer;

import java.util.List;
import java.util.Optional;

/**
 * @author Nikolay Viguro <nikolay.viguro@loyaltyplant.com>, 07.05.18
 */

public interface DeveloperDAO extends JpaRepository<Developer, Long> {
    List<Developer> findByEnabledTrue();

    List<Developer> findAllByEnabledTrue(Pageable pageable);

    Optional<Developer> findByName(String name);
}
