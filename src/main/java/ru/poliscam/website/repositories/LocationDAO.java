package ru.poliscam.website.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.poliscam.website.models.sql.Location;

import java.util.Optional;

/**
 * @author Nikolay Viguro, 11.11.18
 */

public interface LocationDAO extends JpaRepository<Location, Long> {
    Optional<Location> findByName(String name);
}
