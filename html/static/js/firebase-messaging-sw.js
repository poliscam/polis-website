importScripts('https://www.gstatic.com/firebasejs/5.5.8/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/5.5.8/firebase-messaging.js');

console.log("FCM: SW init");

var config = {
    apiKey: "AIzaSyAI96JQGKLSd8EC0pRqknWrqrv7Uh77IzE",
    authDomain: "poliscam-push.firebaseapp.com",
    databaseURL: "https://poliscam-push.firebaseio.com",
    projectId: "poliscam-push",
    storageBucket: "poliscam-push.appspot.com",
    messagingSenderId: "89704194084"
};

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);

    return self.registration.showNotification('PolisCam.Ru', {
        body: payload.notification.body,
        vibrate: [200, 100, 200, 100, 200, 100, 200],
        icon: 'https://poliscam.ru/images/apple-touch-icon-114x114.png',
        tag: 'poliscam-tag'
    });
});