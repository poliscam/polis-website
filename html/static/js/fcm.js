$(document).ready(function () {

    var config = {
        apiKey: "AIzaSyAI96JQGKLSd8EC0pRqknWrqrv7Uh77IzE",
        authDomain: "poliscam-push.firebaseapp.com",
        databaseURL: "https://poliscam-push.firebaseio.com",
        projectId: "poliscam-push",
        storageBucket: "poliscam-push.appspot.com",
        messagingSenderId: "89704194084"
    };

    firebase.initializeApp(config);

    const messaging = firebase.messaging();

    messaging.usePublicVapidKey('BOkpEkIXtiup5sqs9gqf5gYCFvosHNT7L52K4_33zJTHyu85KSZv-tCjOnUhUZ7dQstmKzZW3uBVQE3Z-b6NdRk');

    messaging.requestPermission()
        .then(function () {
            console.log('Notification permission granted.');
            return messaging.getToken()
        })
        .then(function (result) {
            console.log("The token is: ", result);
            fetch('/api/v1/subscription/add',
                {
                    method: 'post', headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    }, body: JSON.stringify({token: result})
                });
        })
        .catch(function (err) {
            console.log('Unable to get permission to notify.', err);
        });

    messaging.onMessage(function (payload) {
        console.log('Message received. ', payload);
        $.notify({
            message: 'Опубликована новость:\n ' + payload.notification.body
        }, {
            type: 'success',
            delay: 0,
            placement: {
                from: "bottom",
                align: "right"
            }
        });
    });

    messaging.onTokenRefresh(async () => {
        console.log('token refreshed');
        const newToken = await messaging.getToken();
        fetch('/api/v1/subscription/add', {
            method: 'post', headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({token: newToken})
        });
    });
});